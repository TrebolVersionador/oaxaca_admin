/* eslint-disable eqeqeq */
// import { useEffect } from "react";
import { useSelector } from "react-redux";

export const useGetArea = (id_area) => {
  const { areas = [] } = useSelector((state) => state.catalogue);

  return areas.find((item) => item.id_area == id_area)?.area ?? "";
};
