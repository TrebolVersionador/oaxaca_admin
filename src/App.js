// import { MinText, Subtitle, Text, Title } from "./assets/styles/Text";
import { ThemeProvider } from "styled-components";
import AppRouter from "./router";
import Theme from "./constants/Theme";
import { BrowserRouter as Router } from "react-router-dom";
import ReduxWrapper from "./store";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider as ThemeMUI } from "@material-ui/core/styles";

const themeMui = createMuiTheme({
  palette: {
    primary: {
      light: "#2ee32b",
      main: "#c5c7c5",
      dark: "#002884",
      contrastText: "#fff",
    },
    secondary: {
      light: "#1F934C",
      main: "#1F934C",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

function App() {
  return (
    <ReduxWrapper>
      <ThemeMUI theme={themeMui}>
        <ThemeProvider theme={Theme}>
          <Router>
            <CssBaseline />
            <AppRouter />
          </Router>
        </ThemeProvider>
      </ThemeMUI>
    </ReduxWrapper>
  );
}

export default App;
