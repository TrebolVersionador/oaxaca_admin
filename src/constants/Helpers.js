export function truncateText(text) {
  if (text?.length <= 30) return text;
  return `${text.substring(0, 40)}...`;
}

export function formatedNumber(number) {
  return number?.toLocaleString();
}

export function youtubeParser(url) {
  var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
  var match = url.match(regExp);
  // eslint-disable-next-line eqeqeq
  return match && match[7].length == 11 ? match[7] : false;
}
