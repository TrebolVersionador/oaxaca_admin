const Theme = {
  primaryColor: "#B7156D",
  secondaryColor: "#1F934C",
  textColor: "#262626",
  grey: "#b3b3b3",
  canvas: "#FFFFFF",
  textButtonColor: "white",
  borderInput: "1.5px solid #1F934C",
};

export default Theme;
