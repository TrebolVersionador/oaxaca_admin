import { CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectStyled } from "../../assets/styles/Forms";
import { getMunicipios } from "../../store/actions/Catalogue";

export default function MunicipioSelect({ name, defaultValue }) {
  const { municipios = [], loadingMunicipios } = useSelector(
    (state) => state.catalogue
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMunicipios());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingMunicipios) return <CircularProgress />;

  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        {loadingMunicipios ? "Cargando..." : "Municipio"}
      </option>
      {React.Children.toArray(
        municipios.map((item) => {
          return <option value={item.id_municipio}>{item.municipio}</option>;
        })
      )}
    </SelectStyled>
  );
}

MunicipioSelect.propTypes = {
  defaultValue: PropTypes.any.isRequired,
  name: PropTypes.any,
};
MunicipioSelect.defaultProps = {
  name: "id_municipio",
};
