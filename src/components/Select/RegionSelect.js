import { CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectStyled } from "../../assets/styles/Forms";
import { getRegions } from "../../store/actions/Catalogue";

export default function RegionSelect({ name, defaultValue }) {
  const { regions = [], loadingRegions } = useSelector(
    (state) => state.catalogue
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRegions());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingRegions) return <CircularProgress />;

  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        {loadingRegions ? "Cargando..." : "Region"}
      </option>
      {React.Children.toArray(
        regions.map((item) => {
          return <option value={item.id_region}>{item.region}</option>;
        })
      )}
    </SelectStyled>
  );
}

RegionSelect.propTypes = {
  defaultValue: PropTypes.any.isRequired,
  name: PropTypes.any,
};
RegionSelect.defaultProps = {
  name: "id_region",
};
