import { CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectStyled } from "../../assets/styles/Forms";
import { getLocations } from "../../store/actions/Catalogue";

export default function LocalidadSelect({ name, defaultValue }) {
  const { locations = [], loadingLocations } = useSelector(
    (state) => state.catalogue
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLocations());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingLocations) return <CircularProgress />;

  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        {loadingLocations ? "Cargango..." : "Localidad"}
      </option>
      {React.Children.toArray(
        locations.map((item) => {
          return <option value={item.id_location}>{item.location}</option>;
        })
      )}
    </SelectStyled>
  );
}

LocalidadSelect.propTypes = {
  defaultValue: PropTypes.any.isRequired,
  name: PropTypes.any,
};
LocalidadSelect.defaultProps = {
  // defaultValue: 0,
  name: "id_location",
};
