import PropTypes from "prop-types";
import React from "react";
import { SelectStyled } from "../../assets/styles/Forms";

export default function PositionSelect({ name, defaultValue }) {
  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        Cargo
      </option>
      {React.Children.toArray(
        regions.map((item) => {
          return <option value={item.id_position}>{item.position}</option>;
        })
      )}
    </SelectStyled>
  );
}

PositionSelect.propTypes = {
  defaultValue: PropTypes.any,
  name: PropTypes.any,
};
PositionSelect.defaultProps = {
  // defaultValue: 0,
  name: "id_position",
};
