import PropTypes from "prop-types";
import React from "react";
import { SelectStyled } from "../../assets/styles/Forms";

export default function StatusSelect({ name, defaultValue }) {
  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        Status
      </option>
      {React.Children.toArray(
        regions.map((item) => {
          return <option value={item.status}>{item.name}</option>;
        })
      )}
    </SelectStyled>
  );
}

StatusSelect.propTypes = {
  defaultValue: PropTypes.any.isRequired,
  name: PropTypes.any,
};
StatusSelect.defaultProps = {
  name: "status",
};

const regions = [
  {
    status: "cumplido",
    name: "Cumplido",
  },
  {
    status: "Comenzado",
    name: "En Proceso",
  },
];
