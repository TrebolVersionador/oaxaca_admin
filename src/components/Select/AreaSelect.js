import { CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectStyled } from "../../assets/styles/Forms";
import { getAreas } from "../../store/actions/Catalogue";

export default function AreaSelect({ name, defaultValue, handleChange }) {
  const { areas = [], loadingAreas } = useSelector((state) => state.catalogue);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAreas());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingAreas) return <CircularProgress />;

  return (
    <SelectStyled
      name={name}
      defaultValue={defaultValue}
      required
      onChange={handleChange}
    >
      <option value={0} disabled>
        {loadingAreas ? "Cargando..." : "Área"}
      </option>
      {React.Children.toArray(
        areas.map((item) => {
          return <option value={item.id_area}>{item.area}</option>;
        })
      )}
    </SelectStyled>
  );
}

AreaSelect.propTypes = {
  defaultValue: PropTypes.any.isRequired,
  name: PropTypes.any,
  handleChange: PropTypes.func,
};
AreaSelect.defaultProps = {
  name: "id_area",
  handleChange: () => {},
};
