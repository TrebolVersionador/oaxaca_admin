import { CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SelectStyled } from "../../assets/styles/Forms";
import { getTitles } from "../../store/actions/Catalogue";

export default function TitleSelect({ name, defaultValue }) {
  const { titles = [], loadingTitles } = useSelector(
    (state) => state.catalogue
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTitles());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (loadingTitles) return <CircularProgress />;

  return (
    <SelectStyled name={name} defaultValue={defaultValue} required>
      <option value={0} disabled>
        {loadingTitles ? "Cargando" : "Título"}
      </option>
      {React.Children.toArray(
        titles.map((item) => {
          return <option value={item.id_title}>{item.title}</option>;
        })
      )}
    </SelectStyled>
  );
}

TitleSelect.propTypes = {
  defaultValue: PropTypes.any,
  name: PropTypes.any,
};
TitleSelect.defaultProps = {
  name: "id_title",
};
