import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { DeleteX } from "../../assets/styles/Buttons";
import { MinText } from "../../assets/styles/Text";
import Image from "./Image";

export default function ReadImages({ images, deleteAction }) {
  return (
    <>
      {images?.length === 0 ? (
        <MinText>No hay imágenes</MinText>
      ) : (
        React.Children.toArray(
          images?.map((item) => {
            return (
              <ContainerImg
                className="col-xs-4
                col-sm-4
                col-md-4
                col-lg-4"
              >
                <Image src={item.url} />
                <DeleteX action={() => deleteAction(item)} />
              </ContainerImg>
            );
          })
        )
      )}
    </>
  );
}

ReadImages.propTypes = {
  deleteAction: PropTypes.func,
  images: PropTypes.array,
};

const ContainerImg = styled.div`
  position: relative;
`;
