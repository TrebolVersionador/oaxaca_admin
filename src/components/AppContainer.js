import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import Menu from "./Menu";

const App = styled.main`
  height: 100vh;
  overflow: hidden;
  display: grid;
  grid-template-columns: repeat(24, 1fr);
  grid-template-rows: repeat(6, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;

  & .side {
    grid-column-start: 1;
    grid-column-end: ${({ isOpen }) => (isOpen ? "5" : "2")};
    grid-row-start: 1;
    grid-row-end: -1;
  }

  & .body {
    grid-area: 1 / ${({ isOpen }) => (isOpen ? "5" : "2")} / 7 / -1;
    background-color: ${({ theme }) => theme.canvas};
  }

  @media screen and (max-width: 1000px) {
    & .side {
      grid-column-end: 3;
    }
    & .body {
      grid-column-start: 3;
    }
  }
`;

const AppContainer = ({ children, isOpen }) => {
  return (
    // side menu expanded (icon + text) or closes (just icon)
    <App isOpen={isOpen}>
      <Menu />
      {/*  page to reander */}
      <div className="body">{children}</div>
    </App>
  );
};

AppContainer.propTypes = {
  children: PropTypes.any,
  isOpen: PropTypes.bool,
};

export default AppContainer;
