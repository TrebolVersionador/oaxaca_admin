import React, { useState } from "react";
import styled from "styled-components";
import { Text } from "../../assets/styles/Text";
import Theme from "../../constants/Theme";
import { Button } from "../Buttons";

const ImagePreview = () => {
  //   const [picture, setPicture] = useState(null);
  const [imgData, setImgData] = useState(null);

  const onChangePicture = (e) => {
    if (e.target.files[0]) {
      //   setPicture(e.target.files[0]);
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setImgData(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <PrevieContent
      className="col-xs-4
    col-sm-4
    col-md-4
    col-lg-4"
    >
      <Button bgColor={Theme.primaryColor}>Agregar imagen</Button>
    </PrevieContent>
  );
};

export default ImagePreview;

const PrevieContent = styled.div`
  border: 2px dashed grey;
  background-color: rgba(150, 150, 150, 0.2);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

/* <input id="profilePic" type="file" onChange={onChangePicture} />
      <img
        className="playerProfilePic_home_tile"
        src={imgData}
        alt="img-preview"
        width="100%"
      /> */
