import React, { useState } from "react";
import { LabelAddImage } from "../../assets/styles/Forms";
import { Button } from "../Buttons";

export default function ReadAndEditImage({
  name,
  defaultImage,
  readOnly,
  ...rest
}) {
  const [file, setFile] = useState(null);

  const onChangePicture = (e) => {
    if (e.target.files && e.target.files[0]) {
      let newFiles = e.target.files[0];
      setFile(newFiles);
    }
  };

  return (
    <LabelAddImage>
      {file === null && (
        <img src={defaultImage} alt="img-video" height="100%" width="100%" />
      )}
      {!readOnly && (
        <>
          {file && (
            <img
              src={file ? URL.createObjectURL(file) : null}
              alt={file ? file.name : null}
              height="100%"
              width="100%"
            />
          )}
          <input
            type="file"
            accept=".jpg, .jpeg"
            name={name}
            onChange={onChangePicture}
            {...rest}
          />
          <Button
            children="CAMBIAR IMAGEN"
            type="small"
            as="div"
            className="floatBtn"
          />
        </>
      )}

      {file && (
        <Button
          children="Quitar"
          onPress={() => {
            setFile(null);
          }}
          type="small"
          as="div"
          className="floatDelete"
        />
      )}
    </LabelAddImage>
  );
}
