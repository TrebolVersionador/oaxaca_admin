import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import { MinText, Subtitle, Text } from "../../assets/styles/Text";
import { Button } from "../Buttons";
import Theme from "../../constants/Theme";
import styled from "styled-components";
import Axios from "axios";
import { apiUrl } from "../../constants/constants";
import { DeleteX } from "../../assets/styles/Buttons";

export default function AddImageComponent({
  isOpen,
  changeModal,
  dataSend,
  callBack,
}) {
  const classes = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [files, setFiles] = useState([]);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    return () => {
      setFiles(null);
      setError("");
      setIsLoading(false);
    };
  }, [isOpen]);

  const fileHandler = (e) => {
    // add image to statet in []
    let newImages = files?.length > 0 ? [...files] : [];
    let newFiles = e.target.files;
    let maxImages = newFiles.length - 1;
    let x = 0;
    while (x <= maxImages) {
      newImages.push(newFiles[x]);
      x++;
    }
    setFiles(newImages);
    e.target.value = null;
  };

  const handleClose = () => {
    changeModal(false);
  };

  const onSend = () => {
    setIsLoading(true);
    const requests = [];
    files.forEach((element) => {
      var form = new FormData();
      form.append(dataSend.nameFile, element);
      form.append(dataSend.name, dataSend.val);
      requests.push(Axios.post(`${apiUrl}${dataSend.endPoint}`, form));
    });

    Axios.all(requests)
      .then(
        Axios.spread(() => {
          setFiles([]);
          callBack();
          setIsLoading(false);
        })
      )
      .catch(() => {
        setError("Algo salió mal, intenta más tarde");
        setFiles([]);
        setIsLoading(false);
        callBack();
      });
  };

  const removeImg = (index) => {
    let nextFiles = [...files];
    nextFiles.splice(index, 1);
    setFiles(nextFiles);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <div className="flex space-between align-center mt-1 mb-1">
        <Subtitle id="simple-modal-title">Agregar imagen</Subtitle>

        <Label>
          <input
            type="file"
            onChange={fileHandler}
            multiple
            accept=".jpg, .jpeg"
          />
          Seleccionar imagenes <i className="material-icons">library_add</i>
        </Label>
      </div>

      <PreviewImgContent className="">
        {files?.length > 0 &&
          files.map((item, index) => {
            return (
              <ImgContent>
                <img
                  src={files ? URL.createObjectURL(item) : null}
                  alt={files ? item.name : null}
                  height="200px"
                  width="100%"
                />
                <DeleteX action={() => removeImg(index)} />
              </ImgContent>
            );
          })}
      </PreviewImgContent>

      {!!error && (
        <div className="flex align-center">
          <Text color="crimson">{error}</Text>
          <MinText color="crimson">
            <i className="material-icons">error</i>
          </MinText>
        </div>
      )}

      <div className="flex space-between align-center mt-1 mb-1">
        <Button
          children="CANCELAR"
          bgColor={Theme.grey}
          onPress={handleClose}
        />
        {files?.length > 0 && (
          <Button
            children={isLoading ? "Enviando...." : "SUBIR IMAGENES"}
            onPress={onSend}
            disabled={isLoading}
          />
        )}
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={isOpen}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}

const Label = styled.label`
  background-color: ${({ theme }) => theme.secondaryColor};
  color: white;
  border-radius: 0.3em;
  font-size: 0.9em;
  padding: 0.5em 1.5em;
  display: flex;
  justify-content: center;
  align-items: center;

  i {
    margin-left: 0.5em;
  }

  input {
    display: none;
  }
`;

const ImgContent = styled.div`
  width: 200px;
  min-width: 200px;
  padding: 0.5em;
  scroll-snap-align: center;
  display: inline-block;
  position: relative;

  & img {
    background-color: lightgray;
  }
`;

/* const DeleteImg = styled.button`
  color: rgba(219, 34, 18, 0.3);
  position: absolute;
  top: 0.5em;
  right: 0.5em;

  &:hover {
    color: rgba(219, 34, 18, 2);
    background-color: rgba(250, 250, 250, 0.2);
  }
`; */

const PreviewImgContent = styled.div`
  width: 100%;

  height: 200px;
  overflow-x: scroll;
  overflow-y: hidden;
  display: flex;

  scroll-padding: 0 50%;
  scroll-snap-type: x mandatory;

  & img {
    object-fit: cover;
  }
`;

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: "40%",
    minWidth: 250,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));
