import React, { useState } from "react";
import { LabelAddImage } from "../../assets/styles/Forms";
import { Button } from "../Buttons";

export default function AddImageForm({ name, width, ...rest }) {
  const [file, setFile] = useState(null);

  const onChangePicture = (e) => {
    let newFiles = e.target.files[0];
    setFile(newFiles);
  };

  return (
    <LabelAddImage width={width}>
      <Button children="AGREGAR IMAGEN" type="small" as="div" />
      <img 
        src={file ? URL.createObjectURL(file) : null}
        alt={file ? file.name : null}
        height="100%"
        width="100%"
      />
      <input
        type="file"
        accept=".jpg, .jpeg"
        name={name}
        onChange={onChangePicture}
        {...rest}
      />
    </LabelAddImage>
  );
}
