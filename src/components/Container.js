import React, { useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Header from "./Headers";
import AppContainer from "./AppContainer";
import { useHistory, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import usePrevious from "../hooks/usePrevious";

const Content = styled.section`
  height: 100%;

  display: grid;
  grid-template-columns: repeat(24, 1fr);
  grid-template-rows: repeat(24, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;

  & .header {
    grid-area: 1 / 1 / 3 / -1;
  }

  & .content {
    grid-area: 3 / 1 / 25 / -1;
    padding: 1em;
    overflow-y: scroll;
  }
`;

const Container = ({ children, isSecondary }) => {
  const location = useLocation();
  const history = useHistory();
  const { isLoggin } = useSelector((state) => state.auth);
  const previousLoggin = usePrevious(isLoggin);

  useEffect(() => {
    if (!isLoggin) {
      if (previousLoggin) {
        return history.replace(`/iniciar-sesion`);
      }
      history.replace(
        `/iniciar-sesion?redirect=${location.pathname.replace("/", "")}`
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname, isLoggin]);

  return (
    <AppContainer isOpen={true}>
      <Content>
        <Header isSecondary={isSecondary} />
        <div className="content">{children}</div>
      </Content>
    </AppContainer>
  );
};

Container.propTypes = {
  Children: PropTypes.any,
  isSecondary: PropTypes.bool,
};

export default Container;

/*
.parent {
display: grid;
grid-template-columns: repeat(24, 1fr);
grid-template-rows: repeat(12, 1fr);
grid-column-gap: 0px;
grid-row-gap: 0px;
}

.div1 { grid-area: 1 / 1 / 3 / 25; }
.div2 { grid-area: 3 / 1 / 13 / 25; }
*/
