import Theme from "../../constants/Theme";
import { Text } from "../../assets/styles/Text";

export function ColDetail({ children }) {
  return (
    <div
      className="col-xs-12
                col-sm-6
                col-md-4
                col-lg-4 mb-1"
    >
      {children}
    </div>
  );
}

export function ColDetailMax({ children }) {
  return (
    <div
      className="col-xs-12
                col-sm-6
                col-md-6
                col-lg-6 mb-1"
    >
      {children}
    </div>
  );
}

export function ColDetailMin({ children }) {
  return (
    <div
      className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3 mb-1"
    >
      {children}
    </div>
  );
}

export function ColTitleDetail({ title, detail }) {
  return (
    <div className="row mb-1">
      <div className="col-xs-12 col-sm-7 col-md-9 col-lg-9">
        <div className="row">
          <div className="col-xs-4 col-sm-3 col-md-4 col-lg-2">
            <SubtitleDetail children={title} />
          </div>
          <div className="col-xs-12 col-sm-9 col-md-8 col-lg-9">{detail}</div>
        </div>
      </div>
    </div>
  );
}

export const ColDescription = ({ children }) => {
  return (
    <div
      className="col-xs-12
                col-sm-12
                col-md-12
                col-lg-12 mb-1"
    >
      {children}
    </div>
  );
};

export const SubtitleDetail = ({ children }) => {
  return (
    <Text
      className="inline"
      color={Theme.secondaryColor}
      weight="bold"
      children={children}
    />
  );
};

export const RowDetail = ({ children }) => {
  return (
    <div className="row" style={{ marginBottom: "0.5em" }}>
      {children}
    </div>
  );
};
