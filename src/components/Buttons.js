import React, { useEffect, useMemo } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ReactComponent as FbIcon } from "../assets/icons/facebook.svg";
import { ReactComponent as GoogleIcon } from "../assets/icons/google.svg";
import { ReactComponent as AppleIcon } from "../assets/icons/apple.svg";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import GoogleLogin from "react-google-login";
import AppleLogin from "react-apple-login";
import { useDispatch } from "react-redux";
import { loginApple, loginFb, loginGoogle } from "../store/actions/Auth";
import { LOGGING_ERROR } from "../store/types/Auth";

const StyledButton = styled.button`
  background-color: ${({ bgColor, theme }) => bgColor ?? theme.secondaryColor};
  font-size: ${({ size }) => size || "0.8em"};
  color: ${({ color, theme }) => color ?? theme.textButtonColor};
  font-weight: ${({ bold = false }) => (!bold ? "normal" : "bold")};
  padding: ${({ large }) => large};
  border-radius: 0.2em;
  box-shadow: 0 0px 5px 1px rgba(100, 100, 100, 0.4);
  font-weight: ${({ weight }) => weight || "normal"};
`;

export const Button = ({ children, isLoading, onPress, type, ...rest }) => {
  let large = useMemo(() => {
    switch (type) {
      case "small":
        return "0.7em 1em";
      case "normal":
        return "0.7em 1.8em";
      case "large":
        return "0.7em 2.5em";
      case "xLarge":
        return "1em 3.5em";
      case "xxLarge":
        return "1.2em 4em";
      default:
        return "0.7em 1.8em";
    }
  }, [type]);

  return (
    <StyledButton
      {...rest}
      large={large}
      disabled={isLoading}
      onClick={onPress}
    >
      {children}
    </StyledButton>
  );
};

Button.propTypes = {
  color: PropTypes.string,
  isLoading: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["small", "normal", "large", "xLarge", "xxLarge"]),
};

Button.defaultProps = {
  onPress: () => {},
  type: "normal",
};

export const FacebookLoginLink = ({ onClick }) => (
  <DotSocial
    bgColor="#3b5998"
    onClick={onClick}
    // as="a"
    // href="http://localhost:3030/api/v1/users/login/facebook"
  >
    <FbIcon width="1.5em" height="1.5em" fill="white" />
  </DotSocial>
);

export const FacebookLoginButton = () => {
  const dispatch = useDispatch();

  const responseFacebook = (response) => {
    if (response?.accessToken) {
      return dispatch(loginFb(response.accessToken));
    }
    dispatch({
      type: LOGGING_ERROR,
      payload: "No se pudo iniciar sesión con Facebook",
    });
  };

  return (
    <FacebookLogin
      appId="1054661791666883"
      autoLoad={false}
      callback={responseFacebook}
      render={(renderProps) => (
        <DotSocial
          bgColor="#3b5998"
          onClick={renderProps.onClick}
          title="Iniciar sesión con Facebook"
        >
          <FbIcon width="1.5em" height="1.5em" fill="white" />
        </DotSocial>
      )}
    />
  );
};

export const GoogleLoginButton = () => {
  const dispatch = useDispatch();

  const responseGoogle = (resp) => {
    if (resp?.tokenId) {
      return dispatch(loginGoogle(resp?.tokenId));
    }
    dispatch({
      type: LOGGING_ERROR,
      payload: "No se pudo iniciar sesión con Google",
    });
  };

  return (
    <GoogleLogin
      clientId="275359607983-7kmb3t5ngnmsqjgrte6svkhf0b98i3cg.apps.googleusercontent.com"
      render={(renderProps) => (
        <DotSocial
          title="Iniciar sesión con Google"
          bgColor="white"
          onClick={renderProps.onClick}
          disabled={renderProps.disabled}
        >
          <GoogleIcon width="1.5em" height="1.5em" />
        </DotSocial>
      )}
      ux_mode="redirect"
      buttonText="Login"
      onSuccess={responseGoogle}
      onFailure={responseGoogle}
      cookiePolicy={"single_host_origin"}
      autoLoad={false}
      redirectUri="http://localhost:3000/"
    />
  );
};

export const AppleLoginButton = () => {
  // const dispatch = useDispatch();

  // /* useEffect(() => {
  //   console.log(window.location);
  //   // console.log();
  //   let hash = window.location.hash;
  //   if (hash.indexOf("id_token") > -1) {
  //     // console.log();
  //     let paramsUrl = window.location.hash.split("&");

  //     paramsUrl.forEach((item) => {
  //       // console.log(item);
  //       if (item.indexOf("id_token") > -1) {
  //         console.log("id_token", item.replace("id_token=", ""));
  //       }
  //     });
  //   }
  //   // var prodId = getParameterByName("code");
  //   // console.log(prodId);
  // }, []); */

  // function getParameterByName(name) {
  //   name = name.replace(/[[]/, "\\[").replace(/[\]]/, "\\]");
  //   var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  //     results = regex.exec(window.location.search);
  //   return results === null
  //     ? ""
  //     : decodeURIComponent(results[1].replace(/\+/g, " "));
  // }

  const appleResponse = (resp) => {
    console.log("---- APPLE ---");
    return console.log(resp);
    // if (resp?.code) {
    //   // return dispatch(loginApple(resp?.code));
    // }
    // dispatch({
    //   type: LOGGING_ERROR,
    //   payload: "No se pudo iniciar sesión con Apple",
    // });
  };

  return (
    <AppleLogin
      responseType="code id_token"
      responseMode="form_post"
      scope="name email"
      clientId="com.gobiernoinforma"
      redirectURI="http://api.gobiernoinforma.com/api/v1/users/login_apple"
      callback={appleResponse}
      render={(renderProps) => (
        <DotSocial bgColor="white" onClick={renderProps.onClick}>
          <AppleIcon width="1.5em" height="1.5em" fill="gray" />
        </DotSocial>
      )}
    />
  );
};

const DotSocial = styled.button`
  width: 2.5em;
  height: 2.5em;
  border-radius: 50%;
  background-color: ${({ bgColor }) => bgColor};
  display: flex;
  justify-content: center;
  align-items: center;

  box-shadow: 1px 1px 3px 0px rgba(110, 100, 110, 0.4);
`;
