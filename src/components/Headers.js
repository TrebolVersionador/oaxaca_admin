import React, { useCallback } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { MinText } from "../assets/styles/Text";
import { ReactComponent as LeftArrow } from "../assets/icons/left.svg";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { CircularProgressIndicator } from "./Loader";

const Header = ({ isSecondary }) => {
  const history = useHistory();
  const { isLoading, userData = {} } = useSelector((state) => state.auth);

  const goBack = useCallback(
    () => {
      history.goBack();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [history]
  );

  return (
    <StyledHeader className="header">
      <div>
        <button onClick={goBack}>
          {isSecondary && (
            <LeftArrow fill="white" width="1.5em" height="1.5em" />
          )}
        </button>
      </div>
      <div>
        {isLoading ? (
          <CircularProgressIndicator />
        ) : (
          <MinText color="white" weight="normal">
            {userData?.email}
          </MinText>
        )}
      </div>
    </StyledHeader>
  );
};

Header.propTypes = {
  isSecondary: PropTypes.bool,
};

export default Header;

const StyledHeader = styled.header`
  background-color: ${({ theme }) => theme.primaryColor};
  padding: 1em;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
