import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";
import Routes from "../router/Routes";
import { ReactComponent as LogoutIcon } from "../assets/icons/exit.svg";
import logo from "../assets/icons/crear_construir_crecer.png";
import logoMob from "../assets/icons/crear_crecer_icon.png";
import { useDispatch } from "react-redux";
import { doLogout } from "../store/actions/Auth";

const MenuStyled = styled.aside`
  background-color: ${({ theme }) => theme.primaryColor};
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(24, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;

  & .header {
    grid-area: 1 / 1 / 3 / -1;
    display: flex;
    justify-content: center;
    align-items: center;

    & img {
      height: 75%;
      display: block;
      margin: 0 auto;
      &.mobile {
        display: none;
      }
    }
  }

  & .menuList {
    grid-area: 4 / 1 / -4 / -1;
  }

  & .closedContent {
    grid-area: -4 / 1 / -1 / -1;
    display: flex;
    align-items: center;
  }

  @media screen and (max-width: 1000px) {
    & .header {
      & img.desk {
        display: none;
      }
      & img.mobile {
        display: block;
      }
    }
  }
  @media screen and (max-width: 600px) {
    & .header {
      & img.mobile {
        width: 2em;
        height: 2em;
      }
    }
  }
`;

const ItemMenu = styled(NavLink)`
  display: flex;
  align-items: center;
  color: white;
  margin-bottom: 0.5em;
  padding: 0.5em 0 0.5em 1em;
  width: 100%;
  font-size: 0.8em;
  text-decoration: none;
  box-sizing: border-box;

  & span {
    margin-left: 0.5em;
  }

  &.active {
    background-color: rgba(250, 250, 250, 0.3);
  }

  @media screen and (max-width: 1000px) {
    justify-content: center;
    padding: 1em 0;

    & span {
      display: none;
    }
  }
`;

const Menu = () => {
  const dispatch = useDispatch();

  const logOut = () => {
    dispatch(doLogout());
  };

  return (
    <MenuStyled className="side">
      <div className="header">
        <img src={logo} alt="logo" className="desk" />
        <img src={logoMob} alt="logo icon" className="mobile" />
      </div>
      <div className="menuList">
        {Routes.filter((itemMenu) => itemMenu.link?.title).map(
          ({ path, link }) => {
            return (
              <ItemMenu to={path} key={`link-${link.title}`} title={link.title}>
                <link.Icon height="1.7em" width="1.7em" fill="white" />
                <span>{link.title}</span>
              </ItemMenu>
            );
          }
        )}
      </div>
      <div className="closedContent">
        <ItemMenu as="button" onClick={logOut}>
          <LogoutIcon height="1.7em" width="1.7em" fill="white" />
          <span>Cerrar sesión</span>
        </ItemMenu>
      </div>
    </MenuStyled>
  );
};

export default Menu;
