import React from "react";
import { CircularProgress } from "@material-ui/core";
import styled from "styled-components";

export default function Loader() {
  return (
    <div>
      <p>Cargando...</p>
    </div>
  );
}

export const CircularProgressIndicator = () => {
  return <CircularProgress color="secondary" />;
};

export const AppLoader = ({ isVisible }) => {
  if (!isVisible) return null;
  return (
    <AppLoaderDiv>
      <CircularProgress color="secondary" />
    </AppLoaderDiv>
  );
};

const AppLoaderDiv = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.4);
  width: 100vw;
  height: 100vh;
  z-index: 100000;
`;
