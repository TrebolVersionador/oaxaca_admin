import PropTypes from "prop-types";
import React from "react";
import MaterialTable from "material-table";

export default function TableComponent({ columns, data }) {
  return (
    <MaterialTable
      columns={columns}
      data={data}
      title=""
      style={{ backgroundColor: "transparent", boxShadow: "0" }}
      actions={exampleActions}
      options={{
        actionsColumnIndex: -1,
      }}
      localization={{
        header: {
          actions: "",
        },
        body: {
          emptyDataSourceMessage: "Sin datos",
          addTooltip: "Agregar",
          deleteTooltip: "Eliminar",
          editTooltip: "Editar",
          filterRow: {
            filterTooltip: "Filtrar",
          },
          editRow: {
            deleteText: "¿Estás seguro de eliminar?",
            cancelTooltip: "Cancelar",
            saveTooltip: "Guardar",
          },
        },
        pagination: {
          labelDisplayedRows: "{from}-{to} de {count}",
          labelRowsSelect: "Filas",
          labelRowsPerPage: "Filas por página",
          firstAriaLabel: "Primera página",
          firstTooltip: "Primera página",
          previousAriaLabel: "Página anterior",
          previousTooltip: "Página anterior",
          nextAriaLabel: "Siguiente Página",
          nextTooltip: "Siguiente Página",
          lastAriaLabel: "Última página",
          lastTooltip: "Última página",
        },
        toolbar: {
          addRemoveColumns: "Agregar o eliminar",
          nRowsSelected: "{0} Fila(s) seleccionadas",
          showColumnsTitle: "Mostrar columnas",
          showColumnsAriaLabel: "Mostrar columnas",
          exportTitle: "Exportar",
          exportAriaLabel: "Exportar",
          exportName: "Export a CSV",
          searchTooltip: "Buscar",
          searchPlaceholder: "Buscar",
        },
      }}
    />
  );
}

const exampleActions = [
  {
    icon: "delete",
    onClick: (_event, rowData) => {
      console.log(rowData);
    },
  },
];

const exampleColumns = [
  { title: "Nombre", field: "name" },
  { title: "Apellido", field: "surname" },
  { title: "Año", field: "birthYear", type: "numeric" },
  {
    title: "Nombre d",
    field: "birthCity",
    lookup: { 34: "sadsa", 63: "da" },
  },
];

const exampleData = [
  {
    name: "San",
    surname: "sadsa",
    birthYear: 1987,
    birthCity: 63,
  },
  {
    name: "adsdsa",
    surname: "dsadas",
    birthYear: 1997,
    birthCity: 63,
  },
];

TableComponent.propTypes = {
  columns: PropTypes.array,
  data: PropTypes.array,
};

TableComponent.defaultProps = {
  columns: exampleColumns,
  data: exampleData,
};
