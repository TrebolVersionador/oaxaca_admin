const localization = {
  header: {
    actions: "",
  },
  body: {
    emptyDataSourceMessage: "Sin datos",
    addTooltip: "Agregar",
    deleteTooltip: "Eliminar",
    editTooltip: "Editar",
    filterRow: {
      filterTooltip: "Filtrar",
    },
    editRow: {
      deleteText: "¿Estás seguro de eliminar?",
      cancelTooltip: "Cancelar",
      saveTooltip: "Guardar",
    },
  },
  pagination: {
    labelDisplayedRows: "{from}-{to} de {count}",
    labelRowsSelect: "Filas",
    labelRowsPerPage: "Filas por página",
    firstAriaLabel: "Primera página",
    firstTooltip: "Primera página",
    previousAriaLabel: "Página anterior",
    previousTooltip: "Página anterior",
    nextAriaLabel: "Siguiente Página",
    nextTooltip: "Siguiente Página",
    lastAriaLabel: "Última página",
    lastTooltip: "Última página",
  },
  toolbar: {
    addRemoveColumns: "Agregar o eliminar",
    nRowsSelected: "{0} Fila(s) seleccionadas",
    showColumnsTitle: "Mostrar columnas",
    showColumnsAriaLabel: "Mostrar columnas",
    exportTitle: "Exportar",
    exportAriaLabel: "Exportar",
    exportName: "Export a CSV",
    searchTooltip: "Buscar",
    searchPlaceholder: "Buscar",
  },
};

export default localization;
