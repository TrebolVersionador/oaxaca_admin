export const exampleActions = [
  {
    icon: "delete",
    onClick: (_event, rowData) => {
      console.log(rowData);
    },
  },
];

export const exampleColumns = [
  { title: "Nombre", field: "name" },
  { title: "Apellido", field: "surname" },
  { title: "Año", field: "birthYear", type: "numeric" },
  {
    title: "Nombre d",
    field: "birthCity",
    lookup: { 34: "sadsa", 63: "da" },
  },
];

export const exampleData = [
  {
    name: "San",
    surname: "sadsa",
    birthYear: 1987,
    birthCity: 63,
  },
  {
    name: "adsdsa",
    surname: "dsadas",
    birthYear: 1997,
    birthCity: 63,
  },
];
