import PropTypes from "prop-types";
import React from "react";
import MaterialTable from "material-table";
import { exampleActions, exampleColumns, exampleData } from "./MockData";
import localization from "./Localization";
import Theme from ".././../constants/Theme";

export default function TableComponent({
  columns,
  data,
  actions,
  onRowClick,
  customLocalization,
}) {
  return (
    <MaterialTable
      columns={columns}
      data={data}
      title=""
      style={{ backgroundColor: "transparent", boxShadow: "0" }}
      actions={actions}
      onRowClick={onRowClick}
      options={{
        rowStyle: {
          fontSize: "0.8em",
          color: `${Theme.dark}`,
        },
        column: { defaultSort: "desc" },
        padding: "dense",
        pageSize: 10,
        actionsCellStyle: {
          color: "grey",
          backgroundColor: "white",
        },
        actionsColumnIndex: -1,
      }}
      localization={{ ...localization, ...customLocalization }}
    />
  );
}

TableComponent.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  actions: PropTypes.array,
  onRowClick: PropTypes.func.isRequired,
};

TableComponent.defaultProps = {
  columns: exampleColumns,
  data: exampleData,
  actions: exampleActions,
};

/*
function SelectedRowStyling() {
  const { useState } = React;
  const [selectedRow, setSelectedRow] = useState(null);
  
  return (
    <MaterialTable
      title="Selected Row Styling Preview"
      columns={[
        { title: 'Name', field: 'name' },
        { title: 'Surname', field: 'surname' },
        { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
        {
          title: 'Birth Place',
          field: 'birthCity',
          lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },
        },
      ]}
      data={[
        { name: 'Mehmet', surname: 'Baran', birthYear: 1987, birthCity: 63 },
        { name: 'Zerya Betül', surname: 'Baran', birthYear: 2017, birthCity: 34 },
      ]}
      onRowClick={((evt, selectedRow) => setSelectedRow(selectedRow.tableData.id))}
      options={{
        rowStyle: rowData => ({
          backgroundColor: (selectedRow === rowData.tableData.id) ? '#EEE' : '#FFF'
        })
      }}
    />
  )
}

*/
