import ForgotPassword from "./ForgotPassword";
import LoginPage from "./LoginPage";
import RecoverPassword from "./RecoverPassword";

const PublicRoutes = [
  {
    name: "Inicio de sesión",
    path: "/iniciar-sesion",
    component: LoginPage,
    isPrivate: false,
  },
  {
    name: "Recuperar contraseña",
    path: "/recuperar-contrasena",
    component: ForgotPassword,
    isPrivate: false,
  },
  {
    name: "Cambiar contraseña",
    path: "/recover",
    component: RecoverPassword,
    isPrivate: false,
  },
];

export default PublicRoutes;
