import React, { useState } from "react";
import { BodyCard, Card, HeaderCard, Page } from "../../assets/styles/Auth";
import logo from "../../assets/icons/crear_construir_crecer.png";
import { InputPassword } from "./Input";
import { Button } from "../../components/Buttons";
import { MinText, Text } from "../../assets/styles/Text";
import { useHistory } from "react-router-dom";
import { apiUrl } from "../../constants/constants";
import { useEffect } from "react";
import useQuery from "../../hooks/useQuery";
// import Axios from "axios";

export default function RecoverPassword() {
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState();
  const history = useHistory();
  let token = useQuery().get("token");

  useEffect(() => {
    setSuccess(false);
    setError("");
  }, []);

  useEffect(() => {
    console.log(token);
  }, [token]);

  const submit = (e) => {
    e.preventDefault();

    const { password, confirm_password } = e.target;

    if (password.value !== confirm_password.value) {
      return setError("La contraseña no coincide");
    }

    console.log(e.target.password.value);
    fetch(`${apiUrl}users/update_password`, {
      method: "POST",
      body: JSON.stringify({ password: e.target.password.value }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${decodeURIComponent(
          escape(window.atob(token))
        )}`,
      },
    })
      .then((res) => res.json())
      .then(({ status, message }) => {
        if (status === "success" && !message) {
          setSuccess(true);
        } else {
          setError(message ?? "Es probable que el enlace haya espirado.");
        }
      })
      .catch(() => {
        setError("Ocurrió un error, intenta de nuevo.");
      });
  };

  return (
    <Page>
      <Card>
        <HeaderCard>
          <img src={logo} alt="logo" className="desk" />
        </HeaderCard>
        <BodyCard>
          {success ? (
            <div>
              <Text children="Inicia sesión con tu nueva contraseña." />
              <br />
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                }}
              >
                <Button
                  color="white"
                  onPress={() => history.goBack()}
                  size="0.8em"
                >
                  Iniciar sesión
                </Button>
              </div>
            </div>
          ) : (
            <form onSubmit={submit}>
              <Text children="Ingresa la nueva contraseña" />
              <br />
              <InputPassword placeholder="Nueva contraseña" />
              <br />
              <InputPassword
                name="confirm_password"
                placeholder="Confirma la nueva contraseña"
              />
              <br />
              <MinText children={error} color="crimson" />
              <br />
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <Button color="white" size="0.8em">
                  Enviar correo
                </Button>
              </div>
            </form>
          )}
        </BodyCard>
      </Card>
    </Page>
  );
}
