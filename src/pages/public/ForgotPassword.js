import React, { useState } from "react";
import { BodyCard, Card, HeaderCard, Page } from "../../assets/styles/Auth";
import logo from "../../assets/icons/crear_construir_crecer.png";
import { InputEmail } from "./Input";
import { Button } from "../../components/Buttons";
import { MinText, Text } from "../../assets/styles/Text";
import { useHistory } from "react-router-dom";
import { apiUrl } from "../../constants/constants";
import { useEffect } from "react";
// import Axios from "axios";

export default function ForgotPassword() {
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState();
  const history = useHistory();

  useEffect(() => {
    setSuccess(false);
    setError("");
  }, []);

  const submit = (e) => {
    e.preventDefault();
    setError("");
    fetch(`${apiUrl}users/recover_password`, {
      method: "POST", // or 'PUT'
      body: JSON.stringify({ email: e.target.email.value }), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then(({ status, message }) => {
        if (status === "success" && !message) {
          setSuccess(true);
        } else {
          setError(message ?? "Revisa el correo que ingresaste");
        }
      })
      .catch(() => {
        setError("Ocurrió un error, intenta de nuevo.");
      });
  };

  return (
    <Page>
      <Card>
        <HeaderCard>
          <img src={logo} alt="logo" className="desk" />
        </HeaderCard>
        <BodyCard>
          {success ? (
            <div>
              <Text children="Revisa tu correo y accede al enlace adjunto para recuorar tu contraseña." />
              <MinText children="Puede tardar en llegar unos minutos. También podría llegar a la carpeta de SPAM" />
              <br />
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  width: "100%",
                }}
              >
                <Button
                  color="white"
                  onPress={() => history.goBack()}
                  size="0.8em"
                >
                  Regresar
                </Button>
              </div>
            </div>
          ) : (
            <form onSubmit={submit}>
              <Text children="Ingresa tu correo. Te enviaremos un enlace para recuperar tu contraseña" />
              <br />
              <InputEmail />
              <br />
              <MinText children={error} color="crimson" />
              <br />
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                }}
              >
                <MinText onClick={() => history.goBack()} className="pointer">
                  Regresar
                </MinText>
                <Button color="white" size="0.8em">
                  Enviar correo
                </Button>
              </div>
            </form>
          )}
        </BodyCard>
      </Card>
    </Page>
  );
}
