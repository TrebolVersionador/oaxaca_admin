import React from "react";
import styled from "styled-components";
import { ReactComponent as EmailIcon } from "../../assets/icons/email.svg";
import { ReactComponent as PassIcon } from "../../assets/icons/padlock.svg";

export function InputEmail() {
  return (
    <InputDiv>
      <LabelInput htmlFor="email">
        <EmailIcon width="1em" height="1em" />
      </LabelInput>
      <Input
        id="email"
        type="email"
        name="email"
        required
        placeholder="Correo electrónico"
      />
    </InputDiv>
  );
}

export function InputPassword({
  name = "password",
  placeholder = "Contraseña",
}) {
  return (
    <InputDiv>
      <LabelInput htmlFor="password">
        <PassIcon width="1em" height="1em" />
      </LabelInput>
      <Input
        id={name}
        type="password"
        name={name}
        required
        placeholder={placeholder}
      />
    </InputDiv>
  );
}

const InputDiv = styled.div`
  padding: 0;
  border: 1px solid grey;
  border-radius: 0.3em;
  display: flex;
  align-items: center;
  width: 100%;
`;

const Input = styled.input`
  border: none;
  outline: none;
  font-size: 0.8em;
  padding: 0.7em 0;
  color: grey;
  width: 100%;
`;

const LabelInput = styled.label`
  padding: 0 0.5em;
`;
