import React, { useEffect, useMemo } from "react";
import MaterialTable from "material-table";
import { useDispatch, useSelector } from "react-redux";
import { PageHeader } from "../../../assets/styles/Page";
import Container from "../../../components/Container";
import localization from "../../../components/TableComponent/Localization";
import {
  addAxles,
  deleteAxles,
  getAllAxles,
  updateAxles,
} from "../../../store/actions/StaticalAnnex";
import { AppLoader } from "../../../components/Loader";
import Theme from "../../../constants/Theme";

export default function Ejes() {
  const { axlesList = [], loadingAxles } = useSelector((state) => state.axles);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllAxles());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const columns = useMemo(
    () => [
      {
        title: "Título",
        field: "title",
      },
      {
        title: "Página",
        field: "page",
      },
    ],
    []
  );

  return (
    <Container>
      <PageHeader title="Temas especiales" />

      <MaterialTable
        title=""
        columns={columns}
        data={axlesList}
        options={{
          rowStyle: {
            fontSize: "0.8em",
            color: Theme.dark,
          },
          column: { defaultSort: "desc" },
          padding: "dense",
          pageSize: 10,
          actionsCellStyle: {
            color: "grey",
            backgroundColor: "white",
          },
          actionsColumnIndex: -1,
        }}
        localization={localization}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              dispatch(addAxles(newData));
              resolve();
            }),
          onRowUpdate: (newData) =>
            new Promise((resolve) => {
              dispatch(updateAxles(newData));
              resolve();
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              dispatch(deleteAxles(oldData.id_axles));
              resolve();
            }),
        }}
      />

      <AppLoader isVisible={loadingAxles} />
    </Container>
  );
}
