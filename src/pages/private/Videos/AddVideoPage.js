import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import AddImageForm from "../../../components/AddImage/AddImageForm";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import { AppLoader } from "../../../components/Loader";
import { youtubeParser } from "../../../constants/Helpers";
import { AddVideo } from "../../../store/actions/Videos";

export default function AddVideoPage() {
  const history = useHistory();
  const [error, setError] = useState("");
  const { isLoading } = useSelector((state) => state.videos);
  const dispatch = useDispatch();

  const sendData = (e) => {
    e.preventDefault();
    setError("");
    const { title, url, image } = e.target;
    const idYou = youtubeParser(url.value);
    if (!idYou) {
      return setError("Url no valida");
    }
    dispatch(
      AddVideo(
        {
          title: title.value,
          url: youtubeParser(url.value),
          image: image.files[0],
        },
        (id_video) => {
          history.replace(`/videos/info/${id_video}`);
        }
      )
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Nuevo video" />
      <form onSubmit={sendData}>
        <InputStyled
          placeholder="Título de video"
          name="title"
          width="100%"
          required
        />
        <InputStyled
          placeholder="Link del video"
          name="url"
          width="100%"
          required
          type="url"
        />
        <AddImageForm name="image" required />{" "}
        <div className="flex justify-center align-center direction-column mt-1">
          <Text color="crimson" className="mb-1">
            {error}
          </Text>
          <Button children="CREAR VIDEO" type="xLarge" />
        </div>
      </form>
      <AppLoader isVisible={isLoading} />
    </Container>
  );
}
