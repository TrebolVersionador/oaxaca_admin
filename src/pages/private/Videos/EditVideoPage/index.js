import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Container from "../../../../components/Container";
import Loader, { AppLoader } from "../../../../components/Loader";
import VideoForm from "./VideoForm";

export default function EditVideoPage() {
  const { id_video } = useParams();
  const [info, setInfo] = useState({});
  const { videosList = [], updating } = useSelector((state) => state.videos);
  const [loadingInfo, setLoadingInfo] = useState(true);

  useEffect(() => {
    // eslint-disable-next-line eqeqeq
    let foundVideo = videosList.find((item) => item.id_video == id_video) ?? {};
    setInfo(foundVideo);
    setLoadingInfo(false);
  }, [id_video, videosList]);

  return (
    <Container isSecondary>
      {loadingInfo ? (
        <Loader />
      ) : !!info.title ? (
        <VideoForm info={info} />
      ) : (
        <p>ERROR</p>
      )}
      <AppLoader isVisible={updating} />
    </Container>
  );
}
