import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled } from "../../../../assets/styles/Forms";
import ReadAndEditImage from "../../../../components/AddImage/ReadAndEditImage";
import { Button } from "../../../../components/Buttons";
import { youtubeParser } from "../../../../constants/Helpers";
import { updateVideo } from "../../../../store/actions/Videos";

export default function VideoForm({ info }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const senData = (e) => {
    e.preventDefault();

    const { title, url, image } = e.target;

    const toSend = {
      ...info,
      title: title.value,
      url: youtubeParser(url.value),
      image: image.files[0],
    };

    dispatch(
      updateVideo(toSend, () => {
        history.goBack();
      })
    );
  };

  return (
    <form onSubmit={senData}>
      <InputStyled
        name="title"
        placeholder="Título"
        defaultValue={info.title}
        width="100%"
      />
      <InputStyled
        name="url"
        type="url"
        defaultValue={`https://www.youtube.com/watch?v=${info.url}`}
        placeholder="Url de video"
      />
      <ReadAndEditImage defaultImage={info.image} name="image" required />

      <div className="centerItems">
        <Button children="ACTUALIZAR" />
      </div>
    </form>
  );
}
