import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import { getAllVideos } from "../../../store/actions/Videos";
import TableVideos from "./TableVideos";

export default function VideosPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(getAllVideos());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onAddPress = () => {
    history.push("/videos/crear")
  };

  return (
    <Container>
      <PageHeader
        title="VIDEOS"
        LeftChild={() => (
          <div>
            <Button children="Agregar nuevo video" onPress={onAddPress} />
          </div>
        )}
      />
      <TableVideos />
    </Container>
  );
}

// <span class="material-icons MuiIcon-root MuiIcon-fontSizeSmall" aria-hidden="true" title="Buscar">search</span>
