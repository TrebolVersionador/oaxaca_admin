import React, { useMemo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Loader from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import { deleteVideo, getAllVideos } from "../../../store/actions/Videos";

export default function TableVideos() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { videosList, isLoading } = useSelector((state) => state.videos);

  useEffect(() => {
    dispatch(getAllVideos());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onRowPress = (_event, rowData) => {
    history.push(`/videos/info/${rowData.id_video}`);
  };

  const onDeletePress = (rowData) => {
    dispatch(deleteVideo(rowData.id_video));
  };

  const TableActions = useMemo(
    () => [
      {
        icon: "delete_outline",
        onClick: (_event, rowData) => onDeletePress(rowData),
        tooltip: "Eliminar",
      },
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  if (isLoading) return <Loader />;
  return (
    <TableComponent
      columns={TableColumns}
      data={videosList}
      actions={TableActions}
      onRowClick={onRowPress}
    />
  );
}

const TableColumns = [
  { title: "#", field: "id_video", maxWidth: "50px", width: "50px" },

  { title: "Título del video", field: "title" },
  {
    title: "Link",
    field: "url",
    render: (rowData) => (
      <a
        href={`https://www.youtube.com/watch?v=${rowData.url}`}
        target="_blanck"
        style={{ width: 50, borderRadius: "50%" }}
      >
        https://www.youtube.com/watch?v= + {rowData.url}
      </a>
    ),
  },
];

/* id_video: 2
image: "http://api.gobiernoinforma.com/videos/video1.jpg"
title: "Cuarto Informe Oaxaca. Semillas de talento
↵"
url: "uMe6Bxu6lrQ" */
