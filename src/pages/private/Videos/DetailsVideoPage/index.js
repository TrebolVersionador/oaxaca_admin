import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Container from "../../../../components/Container";
import Loader from "../../../../components/Loader";
import { getAllVideos } from "../../../../store/actions/Videos";
import RenderVideoInfo from "./RenderVideoInfo";

export default function DetailsVideoPage() {
  const { id_video } = useParams();
  const { videosList = [] } = useSelector((state) => state.videos);
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    if (videosList.length > 0) {
      let foundVideo =
        // eslint-disable-next-line eqeqeq
        videosList.find((item) => item.id_video == id_video) ?? {};
      setInfo(foundVideo);
      setLoadingInfo(false);
    } else {
      dispatch(getAllVideos());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_video, videosList]);

  return (
    <Container isSecondary>
      {loadingInfo ? (
        <Loader />
      ) : !!info.title ? (
        <RenderVideoInfo info={info} />
      ) : (
        <p>ERROR</p>
      )}
    </Container>
  );
}
