import React from "react";
import { Link } from "react-router-dom";
import { PageHeader } from "../../../../assets/styles/Page";
import { Text } from "../../../../assets/styles/Text";
import ReadAndEditImage from "../../../../components/AddImage/ReadAndEditImage";
import { Button } from "../../../../components/Buttons";
import {
  ColDetailMax,
  RowDetail,
  SubtitleDetail,
} from "../../../../components/grid/DetailsComponents";

export default function RenderVideoInfo({ info }) {
  return (
    <section>
      <PageHeader title={info.title} />
      <RowDetail>
        <ColDetailMax>
          <SubtitleDetail eDetail children="Id de video en youtube: " />
          <Text as="span" color="black">
            {info.url}{" "}
          </Text>
          <Text
            as="a"
            href={`https://www.youtube.com/watch?v=${info.url}`}
            target="_blanck"
            color="black"
          >
            Ver en YouTube
          </Text>
        </ColDetailMax>
      </RowDetail>
      <RowDetail>
        <ColDetailMax>
          <SubtitleDetail eDetail children="Imagen de portada" />
        </ColDetailMax>
      </RowDetail>
      <RowDetail>
        <ReadAndEditImage name="image" defaultImage={info.image} readOnly />
      </RowDetail>

      <div className="centerItems">
        <Button
          children="EDITAR"
          as={Link}
          to={`/videos/editar/${info.id_video}`}
        />
      </div>
    </section>
  );
}
