import React, { useMemo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Loader, { AppLoader } from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import { deleteStream, getAllStreams } from "../../../store/actions/Videos";

export default function TableTransmisiones() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { streamsList = [], loadingStreams, uploadingStreams } = useSelector(
    (state) => state.videos
  );

  useEffect(() => {
    dispatch(getAllStreams());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onRowPress = (_event, rowData) => {
    history.push(`/transmisiones/info/${rowData.id_live_cast}`);
  };

  const onDeletePress = (rowData) => {
    dispatch(deleteStream(rowData.id_live_cast));
  };

  const TableActions = useMemo(
    () => [
      {
        icon: "delete_outline",
        onClick: (_event, rowData) => onDeletePress(rowData),
        tooltip: "Eliminar",
      },
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  if (loadingStreams) return <Loader />;
  return (
    <>
      <TableComponent
        columns={TableColumns}
        data={streamsList}
        actions={TableActions}
        onRowClick={onRowPress}
      />
      <AppLoader isVisible={loadingStreams || uploadingStreams} />
    </>
  );
}

const TableColumns = [
  { title: "#", field: "id_live_cast", maxWidth: "50px", width: "50px" },

  { title: "Título del video", field: "title" },
  {
    title: "Link",
    field: "url",
    render: (rowData) => (
      <a
        href={`https://www.youtube.com/watch?v=${rowData.url}`}
        target="_blanck"
        style={{ width: 50, borderRadius: "50%" }}
      >
        https://www.youtube.com/watch?v= + {rowData.url}
      </a>
    ),
  },
];
