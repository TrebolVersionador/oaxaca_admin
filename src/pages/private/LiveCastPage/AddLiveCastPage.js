import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import AddImageForm from "../../../components/AddImage/AddImageForm";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDetailMax,
  ColDetailMin,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import { youtubeParser } from "../../../constants/Helpers";
import { addLiveCast } from "../../../store/actions/Videos";

export default function AddLiveCast() {
  const dispatch = useDispatch();
  const [error, setError] = useState("");
  const history = useHistory();

  const submitForm = (e) => {
    e.preventDefault();
    setError("");
    const { title, url, thumb, date } = e.target;

    let keyYoutube = youtubeParser(url.value);

    if (!keyYoutube) return setError("Url no válido");

    let toSave = {
      title: title.value,
      url: keyYoutube,
      thumb: thumb.files[0],
      date: date.date,
    };

    dispatch(
      addLiveCast(toSave, (id_live_cast) => {
        history.replace(`info/${id_live_cast}`);
      })
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Nueva transmisión" />
      <form onSubmit={submitForm}>
        <RowDetail>
          <ColDetailMax>
            <InputStyled
              name="title"
              required
              placeholder="Título"
              width="100%"
            />
          </ColDetailMax>
          <ColDetailMax>
            <InputStyled
              name="url"
              type="url"
              required
              placeholder="Url"
              width="100%"
            />
          </ColDetailMax>
          <ColDetailMin>
            <InputStyled
              name="date"
              type="date"
              required
              placeholder="Fecha"
              width="100%"
            />
          </ColDetailMin>
        </RowDetail>
        <AddImageForm name="thumb" />
        <div className="flex justify-center align-center direction-column mt-1">
          <Text color="crimson" className="mb-1">
            {error}
          </Text>
          <br />
          <Button type="xLarge" children="Crear transmisión" />
        </div>
      </form>
    </Container>
  );
}
