import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import Container from "../../../components/Container";
import Loader, { AppLoader } from "../../../components/Loader";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import ReadAndEditImage from "../../../components/AddImage/ReadAndEditImage";
import { Button } from "../../../components/Buttons";
import {
  ColDetailMax,
  RowDetail,
  SubtitleDetail,
} from "../../../components/grid/DetailsComponents";
import { getAllStreams } from "../../../store/actions/Videos";

export default function DetailsLiveCastPage() {
  const { id_live_cast } = useParams();
  const { streamsList = [], loadingStreams, uploadingStreams } = useSelector(
    (state) => state.videos
  );
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    if (streamsList.length > 0) {
      let foundVideo =
        streamsList.find(
          (item) => item.id_live_cast === Number(id_live_cast)
        ) ?? {};

      setInfo(foundVideo);
      setLoadingInfo(false);
    } else {
      dispatch(getAllStreams());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_live_cast, streamsList]);

  return (
    <Container isSecondary>
      {loadingInfo ? (
        <Loader />
      ) : !!info.title ? (
        <section>
          <PageHeader title={info.title} />
          <RowDetail>
            <ColDetailMax>
              <SubtitleDetail eDetail children="Id de video en youtube: " />
              <Text as="span" color="black">
                {info.url}{" "}
              </Text>
              <Text
                as="a"
                href={`https://www.youtube.com/watch?v=${info.url}`}
                target="_blanck"
                color="black"
              >
                Ver en YouTube
              </Text>
            </ColDetailMax>
            <ColDetailMax>
              <SubtitleDetail children="Fecha de publicación: " />
              <Text as="span" color="black">
                {info.date.substring(0, 10)}
              </Text>
            </ColDetailMax>
          </RowDetail>
          <RowDetail>
            <ColDetailMax>
              <SubtitleDetail eDetail children="Imagen de portada" />
            </ColDetailMax>
          </RowDetail>
          <RowDetail>
            <ReadAndEditImage name="image" defaultImage={info.image} readOnly />
          </RowDetail>

          <div className="centerItems">
            <Button
              children="EDITAR"
              as={Link}
              to={`/transmisiones/editar/${info.id_live_cast}`}
            />
          </div>
        </section>
      ) : (
        <p>ERROR</p>
      )}
      <AppLoader isVisible={loadingStreams || uploadingStreams} />
    </Container>
  );
}
