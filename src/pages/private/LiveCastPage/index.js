import React from "react";
import { Link } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import TableTransmisiones from "./TableTransmisiones";

export default function StreamsPage() {
  return (
    <Container>
      <PageHeader
        title="Transmisiones"
        LeftChild={() => (
          <div>
            <Button
              children="Agregar nueva transmisiones"
              as={Link}
              to="/transmisiones/crear"
            />
          </div>
        )}
      />
      <TableTransmisiones />
    </Container>
  );
}

// <span class="material-icons MuiIcon-root MuiIcon-fontSizeSmall" aria-hidden="true" title="Buscar">search</span>
