import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import ReadAndEditImage from "../../../components/AddImage/ReadAndEditImage";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDetailMax,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import Loader, { AppLoader } from "../../../components/Loader";
import { youtubeParser } from "../../../constants/Helpers";
import { getAllStreams, updateStream } from "../../../store/actions/Videos";

export default function EditLiveCastPage() {
  const { id_live_cast } = useParams();
  const { streamsList = [], loadingStreams, uploadingStreams } = useSelector(
    (state) => state.videos
  );
  const history = useHistory();
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    if (streamsList.length > 0) {
      let foundVideo =
        streamsList.find(
          (item) => item.id_live_cast === Number(id_live_cast)
        ) ?? {};

      setInfo(foundVideo);
      setLoadingInfo(false);
    } else {
      dispatch(getAllStreams());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_live_cast, streamsList]);

  const senData = (e) => {
    e.preventDefault();

    const { title, url, date, image } = e.target;

    let idYoutube = youtubeParser(url.value);
    console.log("ASS", idYoutube);
    if (!idYoutube) return;
    console.log(idYoutube);

    const toSend = {
      ...info,
      title: title.value,
      url: idYoutube,
      date: date.value,
      thumb: image.files ? image.files[0] : null,
    };
    dispatch(
      updateStream(toSend, () => {
        history.goBack();
      })
    );
  };

  if (loadingInfo) return <Loader />;

  return (
    <Container>
      <form onSubmit={senData}>
        <RowDetail>
          <ColDetailMax>
            <InputStyled
              required
              name="title"
              placeholder="Título"
              defaultValue={info.title}
              width="100%"
            />
          </ColDetailMax>
          <ColDetailMax>
            <InputStyled
              required
              name="url"
              type="url"
              defaultValue={`https://www.youtube.com/watch?v=${info.url}`}
              placeholder="Url de video"
              width="100%"
            />
          </ColDetailMax>
          <ColDetailMax>
            <InputStyled
              required
              name="date"
              type="date"
              defaultValue={info.date}
              placeholder="Fecha de publicación"
            />
          </ColDetailMax>
        </RowDetail>

        <ReadAndEditImage defaultImage={info.image} name="image" />

        <div className="centerItems">
          <Button children="ACTUALIZAR" />
        </div>
      </form>

      <AppLoader isVisible={loadingStreams || uploadingStreams} />
    </Container>
  );
}
