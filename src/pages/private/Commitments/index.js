import React, { useEffect, useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { PageHeader } from "../../../assets/styles/Page";
import Container from "../../../components/Container";
import Loader from "../../../components/Loader";
import { Button } from "../../../components/Buttons";
import {
  getAllCommitments,
  toggleActiveCommitment,
} from "../../../store/actions/Commitments";
import TableComponent from "../../../components/TableComponent";
import { useHistory } from "react-router-dom";

export default function CommitmentsPage() {
  const history = useHistory();
  const { commitments = [], isLoading } = useSelector(
    (state) => state.commitments
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllCommitments());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onAddPress = useCallback(() => {
    history.push("/compromisos/crear");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onDeletePress = (rowData) => {
    dispatch(toggleActiveCommitment({ ...rowData, active: 0 }));
  };

  const onActivePress = (rowData) => {
    dispatch(toggleActiveCommitment({ ...rowData, active: 1 }));
  };

  const onRowClick = (_event, rowData) => {
    // /compromisos/:id_commitment/info
    history.push(`/compromisos/${rowData.id_commitment}/info`);
  };

  const TableActions = useMemo(
    () => [
      (rowData) => ({
        icon: "visibility",
        onClick: (_event, rowData) => onDeletePress(rowData),
        hidden: !rowData.active,
        tooltip: "Ocultar",
        iconProps: {
          color: "secondary",
        },
      }),
      (rowData) => ({
        icon: "visibility_off",
        tooltip: "Mostrar",
        onClick: (_event, rowData) => onActivePress(rowData),
        hidden: !!rowData.active,
        iconProps: {
          color: "disabled",
        },
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <Container>
      <PageHeader
        title="Compromisos"
        LeftChild={() => (
          <Button children="Agregar nuevo compromiso" onPress={onAddPress} />
        )}
      />
      {isLoading ? (
        <Loader />
      ) : (
        <TableComponent
          columns={TableColumns}
          data={commitments}
          actions={TableActions}
          onRowClick={onRowClick}
        />
      )}
    </Container>
  );
}

const TableColumns = [
  { title: "#", field: "id_commitment", maxWidth: "50px", width: "50px" },
  {
    title: "Título del compromiso",
    field: "tittle",
  },
  {
    title: "Región",
    field: "region",
  },
  {
    title: "Municipio",
    field: "municipio",
  },
  {
    title: "Localidad",
    field: "location",
  },
  {
    title: "Status",
    field: "status",
  },
  {
    title: "Me gusta",
    field: "LIKES",
    type: "numeric",
  },
  /* {
    title: "Visibilidad",
    field: "title",
    render: (rowData) => (
      <span style={{ width: 50, borderRadius: "50%" }}>
        {rowData.active ? "Visible" : "Oculto"}
      </span>
    ),
  }, */
];
