import React, { useState } from "react";
import { InputStyled, TextAreaStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import Container from "../../../components/Container";
import MunicipioSelect from "../../../components/Select/MunicipioSelect";
import RegionSelect from "../../../components/Select/RegionSelect";
import LocalidadSelect from "../../../components/Select/LocalidadSelect";
import {
  RowDetail,
  SubtitleDetail,
} from "../../../components/grid/DetailsComponents";
import AddImageForm from "../../../components/AddImage/AddImageForm";
import { MinText, Text } from "../../../assets/styles/Text";
import { Button } from "../../../components/Buttons";
import StatusSelect from "../../../components/Select/StatusSelect";
import { useDispatch, useSelector } from "react-redux";
import { addCommitment } from "../../../store/actions/Commitments";
import { AppLoader } from "../../../components/Loader";
import { useHistory } from "react-router-dom";

export default function AddCommitmentPage() {
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  const { isLoading } = useSelector((state) => state.commitments);

  const uploadCommitment = (e) => {
    e.preventDefault();
    setError("");
    const {
      tittle,
      id_region,
      id_municipio,
      id_location,
      impacto,
      invesment,
      status,
      description,
      lat,
      lon,
      img_1,
      img_2,
      img_3,
      img_4,
    } = e.target;

    if (
      Number(id_region.value) === 0 ||
      Number(id_municipio.value) === 0 ||
      Number(id_location.value) === 0 ||
      !!lat.value ||
      !!lon.value
    ) {
      return setError("Todos los campos son obligatorios");
    }

    let save = {
      tittle: tittle.value,
      id_region: Number(id_region.value),
      id_municipio: id_municipio.value,
      id_location: Number(id_location.value),
      impact: Number(impacto.value),
      investment: Number(invesment.value),
      status: status.value,
      description: description.value,
      latitude: lat.value,
      length: lon.value,
      principal: 1,
    };

    let images = {
      img_1: img_1.files[0],
      img_2: img_2.files[0],
    };

    if (img_3.files.length > 0) {
      images.img_3 = img_3.files[0];
    }

    if (img_4.files.length > 0) {
      images.img_4 = img_4.files[0];
    }

    dispatch(
      addCommitment(save, images, (id_commitment) => {
        history.replace(`/compromisos/${id_commitment}/info`);
      })
    );
    // addImageCommitment(images, 9);
  };

  return (
    <Container isSecondary>
      <PageHeader title="Nuevo compromiso" />
      <form onSubmit={uploadCommitment}>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-12
                col-md-11
                col-lg-10"
          >
            <InputStyled
              name="tittle"
              placeholder="Título del compromiso"
              width="100%"
            />
          </div>
        </RowDetail>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <RegionSelect />
          </div>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <MunicipioSelect defaultValue={0} />
          </div>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <LocalidadSelect defaultValue={0} />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-2
                col-lg-2"
          >
            <InputStyled name="lat" placeholder="Latitud" width="100%" />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-2
                col-lg-2"
          >
            <InputStyled name="lon" placeholder="Longitud" width="100%" />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <InputStyled name="impacto" placeholder="Impacto" width="100%" />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <InputStyled
              name="invesment"
              placeholder="Inversión"
              width="100%"
            />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <StatusSelect name="status" defaultValue={0} />
          </div>
        </RowDetail>

        <RowDetail>
          <div className="col-xs-12">
            <SubtitleDetail>Description:</SubtitleDetail>
            <TextAreaStyled name="description" />
          </div>
        </RowDetail>
        <SubtitleDetail>
          Imágenes: <MinText as="span">(Min 2)</MinText>
        </SubtitleDetail>
        <RowDetail>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_1" width="100%" required />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_2" width="100%" required />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_3" width="100%" />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_4" width="100%" />
          </div>
        </RowDetail>
        <div className="centerItems flex justify-center align-center direction-column">
          <Text color="crimson" children={error} />
          <Button
            children="Guardar información"
            type="xLarge"
            className="mt-1"
          />
        </div>
      </form>
      <AppLoader isVisible={isLoading} />
    </Container>
  );
}
