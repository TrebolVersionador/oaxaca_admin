import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled, TextAreaStyled } from "../../../../assets/styles/Forms";
import { PageHeader } from "../../../../assets/styles/Page";
import { Text } from "../../../../assets/styles/Text";
import AddImageComponent from "../../../../components/AddImage/AddImageComponent";
import { Button } from "../../../../components/Buttons";
import {
  RowDetail,
  SubtitleDetail,
} from "../../../../components/grid/DetailsComponents";
import ReadImages from "../../../../components/ReadImages";
import LocalidadSelect from "../../../../components/Select/LocalidadSelect";
import MunicipioSelect from "../../../../components/Select/MunicipioSelect";
import RegionSelect from "../../../../components/Select/RegionSelect";
import StatusSelect from "../../../../components/Select/StatusSelect";
import Theme from "../../../../constants/Theme";
import { editCommitment } from "../../../../store/actions/Commitments";

export default function FormEdit({ info, callBack, onDeleteImage }) {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [error, setError] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();

  const onPressDeleteImg = (item) => onDeleteImage(item);

  const onSubmitPress = (e) => {
    e.preventDefault();

    setError("");
    const {
      tittle,
      id_region,
      id_municipio,
      id_location,
      impact,
      investment,
      status,
      description,
      lat,
      lon,
    } = e.target;

    if (
      Number(id_region.value) === 0 ||
      Number(id_municipio.value) === 0 ||
      Number(id_location.value) === 0 ||
      !lat.value ||
      !lon.value
    ) {
      return setError("Todos los campos son obligatorios");
    }

    let save = {
      ...info,
      tittle: tittle.value,
      id_region: Number(id_region.value),
      id_municipio: id_municipio.value,
      id_location: Number(id_location.value),
      impact: Number(impact.value),
      investment: Number(investment.value),
      status: status.value,
      description: description.value,
      latitude: lat.value,
      length: lon.value,
      principal: 1,
    };

    dispatch(
      editCommitment(save, () => {
        history.goBack();
      })
    );
  };

  return (
    <div>
      <PageHeader title="Editar compromiso" />
      <form onSubmit={onSubmitPress}>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-12
                col-md-11
                col-lg-10"
          >
            <InputStyled
              required
              name="tittle"
              placeholder="Título del compromiso"
              width="100%"
              defaultValue={info.tittle}
            />
          </div>
        </RowDetail>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <RegionSelect defaultValue={info.id_region} />
          </div>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          > 
            <MunicipioSelect defaultValue={info.id_municipio} />
          </div>
          <div
            className="col-xs-12
                col-sm-4
                col-md-3
                col-lg-3"
          > 
            <LocalidadSelect defaultValue={info.id_location} />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-2
                col-lg-2"
          >
            <InputStyled
              required
              name="lat"
              placeholder="Latitud"
              width="100%"
              defaultValue={info.latitude}
            />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-2
                col-lg-2"
          >
            <InputStyled
              required
              name="lon"
              placeholder="Longitud"
              width="100%"
              defaultValue={info.length}
            />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <InputStyled
              required
              name="impact"
              placeholder="Impacto"
              width="100%"
              defaultValue={info.impact}
            />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <InputStyled
              required
              name="investment"
              placeholder="Inversión"
              width="100%"
              defaultValue={info.investment}
            />
          </div>
          <div
            className="col-xs-12
                col-sm-6
                col-md-3
                col-lg-3"
          >
            <StatusSelect name="status" defaultValue={info.status} />
          </div>
        </RowDetail>

        <RowDetail>
          <div className="col-xs-12">
            <SubtitleDetail>Description:</SubtitleDetail>
            <TextAreaStyled
              required
              name="description"
              defaultValue={info.description}
            />
          </div>
        </RowDetail>

        <div className="centerItems flex justify-center align-center direction-column">
          <Text color="crimson" children={error} />
          <Button
            children="Guardar información"
            type="xLarge"
            className="mt-1"
          />
        </div>
      </form>
      <div className="flex space-between align-center mb-1 mt-1">
        <SubtitleDetail children="Imágenes:" />
        <Button
          bgColor={Theme.primaryColor}
          onPress={() => {
            setIsOpenModal(true);
          }}
        >
          Agregar imagen
        </Button>
      </div>

      <RowDetail>
        <ReadImages images={info.images} deleteAction={onPressDeleteImg} />
      </RowDetail>
      <AddImageComponent
        isOpen={isOpenModal}
        changeModal={setIsOpenModal}
        dataSend={{
          endPoint: "commitments/add_image",
          name: "id_commitment",
          val: info.id_commitment,
          nameFile: "url",
        }}
        callBack={callBack}
      />
    </div>
  );
}
