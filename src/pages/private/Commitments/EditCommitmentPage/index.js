import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Container from "../../../../components/Container";
import Loader from "../../../../components/Loader";
import {
  deleteImageCommitment,
  getAllCommitments,
  getCommitmentById,
} from "../../../../store/actions/Commitments";
import FormEdit from "./FormEdit";

export default function EditCommitmentPage() {
  const { id_commitment } = useParams();
  const { commitments = [] } = useSelector((state) => state.commitments);
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    if (commitments.length > 0) {
      let info = commitments.find(
        // eslint-disable-next-line eqeqeq
        (item) => item.id_commitment == id_commitment
      );
      console.log(info);
      setInfo(info);
      setLoadingInfo(false);
    } else {
      dispatch(getAllCommitments());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_commitment]);
  /*   useEffect(() => {
    getCommitmentInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_commitment]); */

  const getCommitmentInfo = async () => {
    let info = await getCommitmentById(id_commitment);
    const { data, status } = info;
    console.log(data);
    if (status === 200) {
      setLoadingInfo(false);
      setInfo(data);
    } else {
      setLoadingInfo(false);
    }
  };

  const onDeleteImage = async (item) => {
    if (await deleteImageCommitment(item)) {
      let nextImages = info.images.filter(
        (img) => img.id_commitment_image !== item.id_commitment_image
      );
      let nextInfo = { ...info, images: nextImages };
      setInfo(nextInfo);
    }
  };

  if (loadingInfo) <Loader />;

  return (
    <Container isSecondary>
      <FormEdit
        info={info}
        callBack={getCommitmentInfo}
        onDeleteImage={onDeleteImage}
      />
    </Container>
  );
}
