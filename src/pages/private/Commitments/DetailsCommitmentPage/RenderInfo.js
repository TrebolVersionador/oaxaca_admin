import React, { useState } from "react";
import { Link } from "react-router-dom";
import { PageHeader } from "../../../../assets/styles/Page";
import { MinText, Text } from "../../../../assets/styles/Text";
import AddImageComponent from "../../../../components/AddImage/AddImageComponent";
import { Button } from "../../../../components/Buttons";

import {
  ColDetailMin,
  RowDetail,
  SubtitleDetail,
  ColDescription,
} from "../../../../components/grid/DetailsComponents";
import ReadImages from "../../../../components/ReadImages";
import { formatedNumber } from "../../../../constants/Helpers";
import Theme from "../../../../constants/Theme";

const RenderInfo = ({ info, callBack, onDeleteImage }) => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  const onPressDeleteImg = (item) => onDeleteImage(item);

  return (
    <section>
      <PageHeader title={info.tittle} />
      <RowDetail>
        <ColDetailMin>
          <SubtitleDetail children="Región: " />
          <Text className="inline" children={info.region} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Municipio: " />
          <Text className="inline" children={info.municipio} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Localidad: " />
          <Text className="inline" children={info.location} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Latitud: " />
          <Text className="inline" children={info.latitude} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Longitud: " />
          <Text className="inline" children={info.length} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Impacto: " />
          <Text className="inline" children={formatedNumber(info.impact)} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Inversión: " />
          <Text className="inline" children={formatedNumber(info.investment)} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Status: " />
          <Text className="inline" children={info.status} />
        </ColDetailMin>

        <ColDetailMin>
          <SubtitleDetail children="Me gusta totales: " />
          <Text className="inline" children={info.LIKES} />
        </ColDetailMin>
      </RowDetail>
      <br />
      <RowDetail>
        <ColDescription>
          <SubtitleDetail children="Descripción:" />
          <MinText color={Theme.textColor} children={info.description} />
        </ColDescription>
      </RowDetail>
      <br />
      <div className="flex space-between align-center mb-1">
        <SubtitleDetail children="Imágenes:" />
        <Button
          bgColor={Theme.primaryColor}
          onPress={() => {
            setIsOpenModal(true);
          }}
        >
          Agregar imagen
        </Button>
      </div>
      <div className="row">
        <ReadImages images={info.images} deleteAction={onPressDeleteImg} />
      </div>
      <br />
      <br />
      <div style={{ display: "block" }} />
      <div>
        <div className="centerItems ">
          <Button
            as={Link}
            to={`/compromisos/editar/${info.id_commitment}`}
            children="Editar información"
          />
        </div>
      </div>
      <br />
      <AddImageComponent
        isOpen={isOpenModal}
        changeModal={setIsOpenModal}
        dataSend={{
          endPoint: "commitments/add_image",
          name: "id_commitment",
          val: info.id_commitment,
          nameFile: "url",
        }}
        callBack={callBack}
      />
    </section>
  );
};

export default RenderInfo;
