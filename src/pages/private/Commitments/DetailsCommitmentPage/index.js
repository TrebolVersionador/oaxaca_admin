import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Container from "../../../../components/Container";
import Loader from "../../../../components/Loader";
import {
  deleteImageCommitment,
  getCommitmentById,
} from "../../../../store/actions/Commitments";
import RenderInfo from "./RenderInfo";

export default function CommitmentsDetailsPage() {
  const { id_commitment } = useParams();
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);

  useEffect(() => {
    getCommitmentInfo();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_commitment]);

  const getCommitmentInfo = async () => {
    let info = await getCommitmentById(id_commitment);
    const { data, status } = info;
    if (status === 200) {
      setLoadingInfo(false);
      setInfo(data);
    } else {
      setLoadingInfo(false);
    }
  };

  const onDeleteImage = async (item) => {
    if (await deleteImageCommitment(item)) {
      let nextImages = info.images.filter(
        (img) => img.id_commitment_image !== item.id_commitment_image
      );
      let nextInfo = { ...info, images: nextImages };
      setInfo(nextInfo);
    }
  };

  return (
    <Container isSecondary>
      {loadingInfo ? (
        <Loader />
      ) : (
        <RenderInfo
          info={info}
          callBack={getCommitmentInfo}
          onDeleteImage={onDeleteImage}
        />
      )}
    </Container>
  );
}
