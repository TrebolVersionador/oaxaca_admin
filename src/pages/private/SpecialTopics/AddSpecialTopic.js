import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled, TextAreaStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { MinText } from "../../../assets/styles/Text";
import AddImageForm from "../../../components/AddImage/AddImageForm";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDescription,
  RowDetail,
  SubtitleDetail,
} from "../../../components/grid/DetailsComponents";
import { addSpecialTopic } from "../../../store/actions/SpecialTopics";

export default function AddSpecialTopic() {
  const dispatch = useDispatch();
  const history = useHistory();

  const submitData = (e) => {
    e.preventDefault();
    const { title, description, img_1, img_2, img_3, img_4 } = e.target;

    let save = {
      title: title.value,
      description: description.value,
    };

    let images = {
      img_1: img_1.files[0],
      img_2: img_2.files[0],
    };

    if (img_3.files.length > 0) {
      images.img_3 = img_3.files[0];
    }

    if (img_4.files.length > 0) {
      images.img_4 = img_4.files[0];
    }

    dispatch(
      addSpecialTopic(save, images, (id_special_topics) => {
        history.replace(`/temas-especiales/info/${id_special_topics}`);
      })
    );
    // addImageCommitment(images, 9);
  };

  return (
    <Container>
      <PageHeader title="Crear tema" />

      <form onSubmit={submitData}>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-12
                col-md-11
                col-lg-12"
          >
            <InputStyled name="title" placeholder="Título" required />
          </div>
        </RowDetail>
        <br />
        <RowDetail>
          <ColDescription>
            <SubtitleDetail children="Descripción:" />
            <TextAreaStyled
              name="description"
              placeholder="Descripción"
              required
            />
          </ColDescription>
        </RowDetail>
        <br />

        <SubtitleDetail>
          Imágenes: <MinText as="span">(Min 2)</MinText>
        </SubtitleDetail>
        <RowDetail>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_1" width="100%" required />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_2" width="100%" required />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_3" width="100%" />
          </div>
          <div
            className="col-xs-6
                col-sm-4
                col-md-3
                col-lg-3"
          >
            <AddImageForm name="img_4" width="100%" />
          </div>
        </RowDetail>

        <div>
          <div className="centerItems ">
            <Button children="Guardar" type="xLarge" />
          </div>
        </div>
      </form>
    </Container>
  );
}
