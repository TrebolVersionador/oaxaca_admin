import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { MinText, Text } from "../../../assets/styles/Text";
import AddImageComponent from "../../../components/AddImage/AddImageComponent";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  RowDetail,
  ColDetail,
  SubtitleDetail,
  ColDescription,
} from "../../../components/grid/DetailsComponents";
import ReadImages from "../../../components/ReadImages";
import Theme from "../../../constants/Theme";
import { deleteTopicImage } from "../../../store/actions/SpecialTopics";

export default function DetailsSpecialTopic() {
  const { topics = [], isLoading } = useSelector((state) => state.topics);
  const [info, setInfo] = useState({});
  const [isOpenModal, setIsOpenModal] = useState(false);
  const { id_special_topics } = useParams();

  useEffect(() => {
    if (!isLoading) {
      let findItem = topics.find(
        // eslint-disable-next-line eqeqeq
        (item) => item.id_special_topics == id_special_topics
      );
      setInfo(findItem);
    }
  }, [isLoading, id_special_topics, topics]);

  const callBack = () => {
    Axios.get(`special_topics/consult/${id_special_topics}`).then(
      ({ status, data }) => {
        console.log(data);
        if (status === 200) {
          setInfo(data);
        }
      }
    );
  };

  const onPressDeleteImg = async (item) => {
    console.log(item);
    if (await deleteTopicImage(item)) {
      let nextImages = info.images.filter(
        (img) => img.id_image !== item.id_image
      );
      let nextInfo = { ...info, images: nextImages };
      setInfo(nextInfo);
    }
  };

  return (
    <Container isSecondary>
      <PageHeader title={info.title} />
      <RowDetail>
        <ColDetail>
          <SubtitleDetail children="Fecha de publicación: " />
          <Text className="inline" children={info.publication_date} />
        </ColDetail>
      </RowDetail>
      <br />
      <RowDetail>
        <ColDescription>
          <SubtitleDetail children="Descripción:" />
          <MinText color={Theme.textColor} children={info.description} />
        </ColDescription>
      </RowDetail>
      <br />
      <div className="flex space-between align-center mb-1">
        <SubtitleDetail children="Imágenes:" />
        <Button
          bgColor={Theme.primaryColor}
          onPress={() => {
            setIsOpenModal(true);
          }}
        >
          Agregar imagen
        </Button>
      </div>
      <div className="row">
        <ReadImages images={info.images} deleteAction={onPressDeleteImg} />
      </div>
      <br />
      <br />
      <div style={{ display: "block" }} />
      <div>
        <div className="centerItems ">
          <Button
            children="Editar información"
            as={Link}
            to={`/temas-especiales/editar/${info.id_special_topics}`}
          />
        </div>
      </div>
      <br />
      <AddImageComponent
        isOpen={isOpenModal}
        changeModal={setIsOpenModal}
        dataSend={{
          endPoint: "special_topics/add_image",
          name: "id_special_topics",
          val: info.id_special_topics,
          nameFile: "url",
        }}
        callBack={callBack}
      />
    </Container>
  );
}
