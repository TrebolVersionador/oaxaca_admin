import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { InputStyled, TextAreaStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDescription,
  RowDetail,
  SubtitleDetail,
} from "../../../components/grid/DetailsComponents";
import { AppLoader } from "../../../components/Loader";
import { updateSpecialTopic } from "../../../store/actions/SpecialTopics";

export default function EditSpecialTopic() {
  const { topics = [], isLoading, uploading } = useSelector(
    (state) => state.topics
  );
  const [info, setInfo] = useState({});
  const { id_special_topics } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (!isLoading) {
      let findItem = topics.find(
        // eslint-disable-next-line eqeqeq
        (item) => item.id_special_topics == id_special_topics
      );
      setInfo(findItem);
    }
  }, [isLoading, id_special_topics, topics]);

  const submitData = (e) => {
    e.preventDefault();
    const { title, description } = e.target;

    const toSend = {
      ...info,
      title: title.value,
      description: description.value,
    };

    dispatch(
      updateSpecialTopic(toSend, () => {
        history.goBack();
      })
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Editar tema" />
      <form onSubmit={submitData}>
        <RowDetail>
          <div
            className="col-xs-12
                col-sm-12
                col-md-11
                col-lg-12"
          >
            <InputStyled
              name="title"
              placeholder="Título"
              required
              defaultValue={info?.title}
            />
          </div>
        </RowDetail>
        <br />
        <RowDetail>
          <ColDescription>
            <SubtitleDetail children="Descripción:" />
            <TextAreaStyled
              name="description"
              placeholder="Descripción"
              required
              defaultValue={info?.description}
            />
          </ColDescription>
        </RowDetail>
        <div className="centerItems ">
          <Button children="Guardar" type="xLarge" />
        </div>
      </form>
      <AppLoader isVisible={uploading} />
    </Container>
  );
}
