import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import { AppLoader } from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import {
  getAllTopics,
  updateSpecialTopic,
} from "../../../store/actions/SpecialTopics";

export default function SpecialThemes() {
  const { topics = [], isLoading } = useSelector((state) => state.topics);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(getAllTopics());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onRowPress = (_ev, data) => {
    history.push(`/temas-especiales/info/${data.id_special_topics}`)
  };
  const onDeletePress = (data) => {
    dispatch(updateSpecialTopic({ ...data, active: 0 }));
  };

  const onActivePress = (data) => {
    dispatch(updateSpecialTopic({ ...data, active: 1 }));
  };

  const TableActions = useMemo(
    () => [
      (rowData) => ({
        icon: "visibility",
        onClick: (_event, rowData) => onDeletePress(rowData),
        hidden: !rowData.active,
        disabled: !rowData.active,
        tooltip: "Ocultar",
        iconProps: {
          color: "secondary",
        },
      }),
      (rowData) => ({
        icon: "visibility_off",
        tooltip: "Mostrar",
        onClick: (_event, rowData) => onActivePress(rowData),
        hidden: !!rowData.active,
        disabled: !!rowData.active,
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <Container>
      <PageHeader
        title="Temas especiales"
        LeftChild={() => (
          <Button
            as={Link}
            to="/temas-especiales/crear"
            children="Agregar nuevo tema"
          />
        )}
      />
      <TableComponent
        data={topics}
        columns={TableColumns}
        actions={TableActions}
        onRowClick={onRowPress}
      />
      <AppLoader isVisible={isLoading} />
    </Container>
  );
}

const TableColumns = [
  { title: "#", field: "id_special_topics", maxWidth: "50px", width: "50px" },
  {
    title: "Título de la notica",
    field: "title",
  },
  {
    title: "Fecha de publicación",
    field: "date_post",
  },
];
