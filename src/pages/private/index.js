// export private routes to router
// VIDEOS
import VideosPage from "./Videos";
import AddVideoPage from "./Videos/AddVideoPage";
import EditVideoPage from "./Videos/EditVideoPage";
import DetailsVideoPage from "./Videos/DetailsVideoPage";
import { ReactComponent as VideosIcon } from "../../assets/icons/camera.svg";
// News
import { ReactComponent as NewIcon } from "../../assets/icons/news.svg";
import NewsPage from "./News";
import AddNewsPage from "./News/AddNewsPage";
import EditNewsPage from "./News/EditNewsPage";
import NewsDetailsPage from "./News/NewsDetailsPage";
// compromisos
import { ReactComponent as CommitmentsIcon } from "../../assets/icons/compass.svg";
import CommitmentsPage from "./Commitments";
import AddCommitmentPage from "./Commitments/AddCommitmentPage";
import DetailsCommitmentPage from "./Commitments/DetailsCommitmentPage";
import EditCommitmentPage from "./Commitments/EditCommitmentPage";
// streams
import StreamsPage from "./LiveCastPage";
import AddLiveCastPage from "./LiveCastPage/AddLiveCastPage";
import DetailsLiveCastPage from "./LiveCastPage/DetailsLiveCastPage";
// funcionarios
import { ReactComponent as DirectoryIcon } from "../../assets/icons/directory.svg";
import DirectoryPage from "./Officers";
import AddOfficerPage from "./Officers/AddOfficerPage";
import DetailsOfficerPage from "./Officers/DetailsOfficerPage";
import EditOfficerPage from "./Officers/EditOfficerPage";
// ejes
import { ReactComponent as BookmarkIcon } from "../../assets/icons/bookmark.svg";
import EjesPage from "./Ejes";
// temas especiales
import { ReactComponent as SpecialIcon } from "../../assets/icons/information.svg";
import SpecialTopics from "./SpecialTopics";
import DetailsSpecialTopic from "./SpecialTopics/DetailsSpecialTopic";
import AddSpecialTopic from "./SpecialTopics/AddSpecialTopic";
import EditSpecialTopic from "./SpecialTopics/EditSpecialTopic";
// Anexo estadísticp
import StaticalAnnex from "./StaticalAnnex";
// USUARIOS
import UsersPage from "./Users";
import { ReactComponent as UsersIcon } from "../../assets/icons/user.svg";
import Detailsuserpage from "./Users/DetailsUserPage";
import EditUserPage from "./Users/EditUserPage";
import EditLiveCastPage from "./LiveCastPage/EditLiveCastPage";

const PrivateRoutes = [
  // usuarios
  {
    name: "usuarios",
    path: "/usuarios",
    component: UsersPage, // page
    isPrivate: true, // only authenticated users
    // ignrore "link if you don't want to see it in the menu
    link: {
      title: "Usuarios", // title on menu
      Icon: UsersIcon, // title on menu
    },
  },
  {
    name: "Información de usuario",
    path: "/usuarios/info/:id_user",
    component: Detailsuserpage, // page
    isPrivate: true, // only authenticated users
    isSecondary: true,
  },
  {
    name: "Información de usuario",
    path: "/usuarios/editar/:id_user",
    component: EditUserPage, // page
    isPrivate: true, // only authenticated users
    isSecondary: true,
  },
  // noticias
  {
    name: "Noticias",
    path: "/noticias",
    component: NewsPage, // page
    isPrivate: true, // only authenticated users
    // ignrore "link if you don't want to see it in the menu
    link: {
      title: "Noticias", // title on menu
      Icon: NewIcon, // title on menu
    },
  },
  {
    name: "Detalles de noticia",
    path: "/noticias/info/:id_new",
    component: NewsDetailsPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Detalles de noticia",
    path: "/noticias/editar/:id_new",
    component: EditNewsPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Agregar noticia",
    path: "/noticias/crear",
    component: AddNewsPage,
    isSecondary: true,
    isPrivate: true,
  },
  // compromisos
  {
    name: "Compromisos",
    path: "/compromisos",
    component: CommitmentsPage,
    isPrivate: true,
    link: {
      title: "Compromisos",
      Icon: CommitmentsIcon,
    },
  },
  {
    name: "Detalles de compromiso",
    path: "/compromisos/:id_commitment/info",
    component: DetailsCommitmentPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Detalles de compromiso",
    path: "/compromisos/editar/:id_commitment",
    component: EditCommitmentPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Agregar compromiso",
    path: "/compromisos/crear",
    component: AddCommitmentPage,
    isSecondary: true,
    isPrivate: true,
  },
  // funcionarios
  {
    name: "Directorio",
    path: "/directorio-funcionarios",
    component: DirectoryPage,
    isPrivate: true,
    link: {
      title: "Direcorio de funcionarios",
      Icon: DirectoryIcon,
    },
  },
  {
    name: "Directorio",
    path: "/directorio-funcionarios/crear",
    component: AddOfficerPage,
    isPrivate: true,
    isSecondary: true,
  },
  {
    name: "Directorio",
    path: "/directorio-funcionarios/info/:id_officer",
    component: DetailsOfficerPage,
    isPrivate: true,
    isSecondary: true,
  },
  {
    name: "Directorio",
    path: "/directorio-funcionarios/editar/:id_officer",
    component: EditOfficerPage,
    isPrivate: true,
    isSecondary: true,
  },
  // ejes
  {
    name: "ejes",
    path: "/ejes",
    component: EjesPage,
    isPrivate: true,
    link: {
      title: "Ejes",
      Icon: BookmarkIcon,
    },
  },
  // anexo
  {
    name: "anexo estadístico",
    path: "/anexo-estadistico",
    component: StaticalAnnex,
    isPrivate: true,
    link: {
      title: "Anexo Estadistico",
      Icon: BookmarkIcon,
    },
  },
  {
    name: "Videos",
    path: "/videos",
    component: VideosPage,
    isSecondary: false,
    isPrivate: true,
    link: {
      title: "Videos",
      Icon: VideosIcon,
    },
  },
  {
    name: "Videos",
    path: "/videos/crear",
    component: AddVideoPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Videos",
    path: "/videos/editar/:id_video",
    component: EditVideoPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Videos",
    path: "/videos/info/:id_video",
    component: DetailsVideoPage,
    isSecondary: true,
    isPrivate: true,
  },
  // stream - transmisiones
  {
    name: "Transmisiones",
    path: "/transmisiones",
    component: StreamsPage,
    isSecondary: false,
    isPrivate: true,
    link: {
      title: "Transmisiones",
      Icon: VideosIcon,
    },
  },
  {
    name: "Transmisiones",
    path: "/transmisiones/crear",
    component: AddLiveCastPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Transmisiones info",
    path: "/transmisiones/info/:id_live_cast",
    component: DetailsLiveCastPage,
    isSecondary: true,
    isPrivate: true,
  },
  {
    name: "Transmisiones info",
    path: "/transmisiones/editar/:id_live_cast",
    component: EditLiveCastPage,
    isSecondary: true,
    isPrivate: true,
  },
  // temas especiales
  {
    name: "Temas especiales",
    path: "/temas-especiales",
    component: SpecialTopics,
    isPrivate: true,
    link: {
      title: "Temas especiales",
      Icon: SpecialIcon,
    },
  },
  {
    name: "Temas especiales",
    path: "/temas-especiales/info/:id_special_topics",
    component: DetailsSpecialTopic,
    isPrivate: true,
    isSecondary: true,
  },
  {
    name: "Temas especiales",
    path: "/temas-especiales/crear",
    component: AddSpecialTopic,
    isPrivate: true,
    isSecondary: true,
  },
  {
    name: "Temas especiales",
    path: "/temas-especiales/editar/:id_special_topics",
    component: EditSpecialTopic,
    isPrivate: true,
    isSecondary: true,
  },
];

export default PrivateRoutes;
