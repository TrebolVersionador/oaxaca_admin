import React, { useMemo, useEffect } from "react";
import MaterialTable from "material-table";
import { PageHeader } from "../../../assets/styles/Page";
import Container from "../../../components/Container";
import localization from "../../../components/TableComponent/Localization";
import { useDispatch, useSelector } from "react-redux";
import { AppLoader } from "../../../components/Loader";
import {
  addStaticalAnnex,
  deleteStaticalAnnex,
  getAllStaticalAnnex,
  updateStaticalAnnex,
} from "../../../store/actions/StaticalAnnex";
// import TableComponent from "../../../components/TableComponent";

export default function StaticalAnnex() {
  const { staticalAnnex = [], loadingStatical } = useSelector(
    (state) => state.axles
  );
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllStaticalAnnex());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const columns = useMemo(
    () => [
      {
        title: "Título",
        field: "title",
      },
      {
        title: "Página",
        field: "page",
      },
    ],
    []
  );

  return (
    <Container>
      <PageHeader title="Temas especiales" />

      <MaterialTable
        title=""
        columns={columns}
        data={staticalAnnex}
        options={{
          rowStyle: {
            fontSize: "0.8em",
            // color: `${Theme.dark}`,
          },
          column: { defaultSort: "desc" },
          padding: "dense",
          pageSize: 10,
          actionsCellStyle: {
            color: "grey",
            backgroundColor: "white",
          },
          actionsColumnIndex: -1,
          // rowStyle: {
          //   fontSize: "0.8em",
          //   color: `${Theme.dark}`,
          // },
        }}
        localization={localization}
        // dispatch(addStaticalAnnex(newData))
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              dispatch(addStaticalAnnex(newData));
              resolve();
            }),
          onRowUpdate: (newData) =>
            new Promise((resolve) => {
              dispatch(updateStaticalAnnex(newData));
              resolve();
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              dispatch(deleteStaticalAnnex(oldData.id_technical_annex));
              resolve();
            }),
        }}
      />

      <AppLoader isVisible={loadingStatical} />
    </Container>
  );
}
