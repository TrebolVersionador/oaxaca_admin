import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  RowDetail,
  ColDetail,
  ColDetailMax,
  ColDescription,
  ColDetailMin,
} from "../../../components/grid/DetailsComponents";
import { AppLoader } from "../../../components/Loader";
import AreaSelect from "../../../components/Select/AreaSelect";
import TitleSelect from "../../../components/Select/TitleSelect";
import { editOfficer } from "../../../store/actions/Directory";

export default function EditOfficerPage() {
  const { officers = [], isLoading } = useSelector((state) => state.directory);
  const dispatch = useDispatch();
  const [error, setError] = useState("");
  const history = useHistory();
  const [info, setInfo] = useState({});
  const { id_officer } = useParams();

  useEffect(() => {
    if (!isLoading) {
      // eslint-disable-next-line eqeqeq
      let findInfo = officers.find((item) => item.id_officer == id_officer);
      setInfo(findInfo);
    }
  }, [officers, isLoading, id_officer]);

  const onSavePress = (e) => {
    e.preventDefault();
    setError("");
    const { name, id_title, phone, id_area, address, position } = e.target;
    if (Number(id_title?.value) === 0 || Number(id_area?.value) === 0) {
      return setError("Todos los datos son requeridos");
    }

    const toSave = {
      ...info,
      name: name.value,
      id_title: id_title?.value,
      position: position.value,
      phone: phone.value,
      id_area: id_area.value,
      address: address.value,
    };
    dispatch(
      editOfficer(toSave, () => {
        history.replace(`/directorio-funcionarios/info/${info.id_officer}`);
      })
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Editar información" />
      {info.title ? (
        <form onSubmit={onSavePress}>
          <RowDetail>
            <ColDetailMin>
              <TitleSelect defaultValue={info?.id_title} />
            </ColDetailMin>
            <ColDetail>
              <InputStyled
                required
                placeholder="Nombre"
                defaultValue={info?.name}
                name="name"
                width="100%"
              />
            </ColDetail>
          </RowDetail>
          <RowDetail>
            <ColDescription>
              <InputStyled
                required
                placeholder="Cargo"
                defaultValue={info?.position}
                name="position"
                width="100%"
              />
            </ColDescription>
          </RowDetail>
          <RowDetail>
            <ColDescription>
              <InputStyled
                required
                placeholder="Dirección"
                defaultValue={info?.address}
                name="address"
                width="100%"
              />
            </ColDescription>
          </RowDetail>
          <RowDetail>
            <ColDetailMax>
              <InputStyled
                required
                placeholder="Teléfono"
                defaultValue={info?.phone}
                name="phone"
                width="100%"
              />
            </ColDetailMax>
          </RowDetail>

          <RowDetail>
            <ColDetail>
              <AreaSelect defaultValue={info.id_area} />
            </ColDetail>
          </RowDetail>

          <div className="centerItems flex justify-center align-center direction-column">
            <Text color="crimson" children={error} />
            <Button children="Crear" type="xLarge" className="mt-1" />
          </div>
        </form>
      ) : (
        <AppLoader />
      )}
    </Container>
  );
}
