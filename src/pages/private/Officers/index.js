import React, { useMemo } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import Loader from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import {
  getAllDirectory,
  toggleDeleteOfficer,
} from "../../../store/actions/Directory";

export default function DirectoryPage() {
  const { officers = [], isLoading } = useSelector((state) => state.directory);
  const dispatch = useDispatch();
  const history = useHistory();
 
  useEffect(() => {
    dispatch(getAllDirectory());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onDeletePress = (dat) => {
    dispatch(toggleDeleteOfficer(dat.id_officer, 0));
  };

  const onActivePress = (dat) => {
    dispatch(toggleDeleteOfficer(dat.id_officer, 1));
  };

  const onRowPress = (_ev, dat) => {
    history.push(`/directorio-funcionarios/info/${dat.id_officer}`);
  };

  const tableActions = useMemo(
    () => [
      (rowData) => ({
        icon: "visibility",
        onClick: (_event, rowData) => onDeletePress(rowData),
        hidden: !rowData.active,
        disabled: !rowData.active,
        tooltip: "Ocultar",
        iconProps: {
          color: "secondary",
        },
      }),
      (rowData) => ({
        icon: "visibility_off",
        tooltip: "Mostrar",
        onClick: (_event, rowData) => onActivePress(rowData),
        hidden: !!rowData.active,
        disabled: !!rowData.active,
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const TableColumns = useMemo(
    () => [
      {
        title: "#",
        field: "id_officer",
        maxWidth: "50px",
        width: "50px",
      },
      { title: "Nombre", field: "name" },
      { title: "Título", field: "title" },
      { title: "Cargo", field: "position" },
      { title: "Teléfono", field: "phone" },
      { title: "Área", field: "area" },
    ],
    []
  );

  return (
    <Container>
      <PageHeader
        title="DIRECTORIO DE FUNCIONARIOS"
        LeftChild={() => (
          <Button
            type="xLarge"
            children="Agregar nuevo funcionario"
            as={Link}
            to="/directorio-funcionarios/crear"
          />
        )}
      />
      {isLoading ? (
        <Loader />
      ) : (
        <TableComponent
          columns={TableColumns}
          data={officers}
          actions={tableActions}
          onRowClick={onRowPress}
        />
      )}
    </Container>
  );
}
