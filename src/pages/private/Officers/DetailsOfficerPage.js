import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import { ColTitleDetail } from "../../../components/grid/DetailsComponents";

export default function DetailsOfficerPage() {
  const { officers = [], isLoading } = useSelector((state) => state.directory);
  const [info, setInfo] = useState({});
  const { id_officer } = useParams();

  useEffect(() => {
    if (!isLoading) {
      // eslint-disable-next-line eqeqeq
      let findInfo = officers.find((item) => item.id_officer == id_officer);
      setInfo(findInfo);
    }
  }, [officers, isLoading, id_officer]);

  return (
    <Container isSecondary>
      <PageHeader title={info?.name} />

      <ColTitleDetail title="Título:" detail={info?.title} />
      <ColTitleDetail title="Cargo:" detail={info?.position} />
      <ColTitleDetail title="Domicilio:" detail={info?.address} />
      <ColTitleDetail title="Teléfono:" detail={info?.phone} />
      <ColTitleDetail title="Área:" detail={info?.area} />
      <div className="centerItems">
        <Button
          children="Editar información"
          as={Link}
          to={`/directorio-funcionarios/editar/${id_officer}`}
        />
      </div>
    </Container>
  );
}
