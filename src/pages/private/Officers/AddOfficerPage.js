import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDescription,
  ColDetail,
  ColDetailMax,
  ColDetailMin,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import AreaSelect from "../../../components/Select/AreaSelect";
import TitleSelect from "../../../components/Select/TitleSelect";
import { addOfficer } from "../../../store/actions/Directory";

export default function AddOfficerPage() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [error, setError] = useState("");

  const onSubmitData = (e) => {
    e.preventDefault();
    setError("");
    const { name, id_title, phone, id_area, address, position } = e.target;
    if (Number(id_title?.value) === 0 || Number(id_area?.value) === 0) {
      return setError("Todos los datos son requeridos");
    }

    const toSave = {
      name: name.value,
      id_title: id_title?.value,
      position: position.value,
      phone: phone.value,
      id_area: id_area.value,
      address: address.value,
    };
    dispatch(
      addOfficer(toSave, (id_officer) => {
        history.replace(`/directorio-funcionarios/info/${id_officer}`);
      })
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Nuevo funcionario" />
      <form onSubmit={onSubmitData}>
        <RowDetail>
          <ColDetailMin>
            <TitleSelect defaultValue={0} />
          </ColDetailMin>
          <ColDetail>
            <InputStyled
              required
              placeholder="Nombre"
              name="name"
              width="100%"
            />
          </ColDetail>
        </RowDetail>
        <RowDetail>
          <ColDescription>
            <InputStyled
              required
              placeholder="Cargo"
              name="position"
              width="100%"
            />
          </ColDescription>
        </RowDetail>
        <RowDetail>
          <ColDescription>
            <InputStyled
              required
              placeholder="Dirección"
              name="address"
              width="100%"
            />
          </ColDescription>
        </RowDetail>
        <RowDetail>
          <ColDetailMax>
            <InputStyled
              required
              placeholder="Teléfono"
              name="phone"
              width="100%"
            />
          </ColDetailMax>
        </RowDetail>

        <RowDetail>
          <ColDetail>
            <AreaSelect defaultValue={0} />
          </ColDetail>
        </RowDetail>

        <div className="centerItems flex justify-center align-center direction-column">
          <Text color="crimson" children={error} />
          <Button children="Crear" type="xLarge" className="mt-1" />
        </div>
      </form>
    </Container>
  );
}
