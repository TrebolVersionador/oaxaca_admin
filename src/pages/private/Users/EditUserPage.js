import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { InputStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDetail,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import Loader, { AppLoader } from "../../../components/Loader";
import { getUsers, updateUser } from "../../../store/actions/Users";

export default function EditUserPage() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { users = [], loadingUsers, uploadingUsers } = useSelector(
    (state) => state.users
  );
  const [info, setInfo] = useState({});
  const { id_user } = useParams();

  useEffect(() => {
    console.log(info);
  }, [info]);

  useEffect(() => {
    if (users.length > 0) {
      // eslint-disable-next-line eqeqeq
      let findUser = users.find((item) => item.id_user == id_user);
      if (findUser) {
        setInfo(findUser);
      }
    } else {
      dispatch(getUsers(true));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [users, loadingUsers, id_user]);

  const submitData = (e) => {
    e.preventDefault();

    const { name, phone, birthdate } = e.target;

    let toSend = {
      ...info,
      name: name.value,
      phone: phone.value ?? "",
      birthdate: birthdate.value,
    };

    dispatch(
      updateUser(toSend, () => {
        history.goBack();
      })
    );
  };

  if (loadingUsers) return <Loader />;
  return (
    <Container isSecondary>
      <PageHeader title="Editar usuario" />
      <form onSubmit={submitData}>
        <RowDetail>
          <ColDetail>
            <InputStyled
              required
              name="name"
              placeholder="Nombre"
              defaultValue={info.name}
              width="100%"
            />
          </ColDetail>
          <ColDetail>
            <InputStyled
              required
              name="birthdate"
              placeholder="Nombre"
              defaultValue={info.birthdate}
              type="date"
              width="100%"
            />
          </ColDetail>
          <ColDetail>
            <InputStyled
              name="phone"
              placeholder="Teléfono"
              defaultValue={info.phone}
              type="tel"
              width="100%"
            />
          </ColDetail>
        </RowDetail>
        <div className="centerItems">
          <Button type="xLarge" children="Editar información" />
        </div>
      </form>
      <AppLoader isVisible={uploadingUsers} />
    </Container>
  );
}
