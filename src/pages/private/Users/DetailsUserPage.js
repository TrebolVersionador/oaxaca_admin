import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Text } from "../../../assets/styles/Text";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDetail,
  RowDetail,
  SubtitleDetail,
} from "../../../components/grid/DetailsComponents";
import Loader, { AppLoader } from "../../../components/Loader";
import { getUsers } from "../../../store/actions/Users";

export default function Detailsuserpage() {
  const dispatch = useDispatch();
  const { users = [], loadingUsers, uploadingUsers } = useSelector(
    (state) => state.users
  );
  const [info, setInfo] = useState({});
  const { id_user } = useParams();

  useEffect(() => {
    console.log(info);
  }, [info]);

  useEffect(() => {
    if (users.length > 0) {
      // eslint-disable-next-line eqeqeq
      let findUser = users.find((item) => item.id_user == id_user);
      if (findUser) {
        setInfo(findUser);
      }
    } else {
      dispatch(getUsers(true));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [users, loadingUsers, id_user]);

  if (loadingUsers) return <Loader />;
  return (
    <Container isSecondary>
      <PageHeader title={info.name} />

      <RowDetail>
        <ColDetail>
          <SubtitleDetail children="Correo: " />
          <Text children={info.email} as="span" />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Fecha de nacimiento: " />
          <Text children={info.birthdate} as="span" />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Teléfono: " />
          <Text children={info.phone} as="span" />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Tipo de registro: " />
          <Text children={info.register_type} as="span" />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Rol de usuario: " />
          <Text
            children={info.role_id === 1 ? "Administrador" : "Usuario"}
            as="span"
          />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Id de usuario: " />
          <Text children={info.id_user} as="span" />
        </ColDetail>
        <ColDetail>
          <SubtitleDetail children="Fecha de registrp: " />
          <Text children={getRegiterTime(info.register_date)} as="span" />
        </ColDetail>
      </RowDetail>

      <div className="centerItems">
        <Button
          type="xLarge"
          children="Editar información"
          as={Link}
          to={`/usuarios/editar/${id_user}`}
        />
      </div>

      <AppLoader isVisible={uploadingUsers} />
    </Container>
  );
}

const getRegiterTime = (date) => {
  if (!date) return "";

  return date?.substring(0, 10);
};
