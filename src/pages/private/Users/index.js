import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import Container from "../../../components/Container";
import { AppLoader } from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import { getUsers, updateUser } from "../../../store/actions/Users";

export default function UsersPage() {
  const dispatch = useDispatch();
  const { users = [], loadingUsers } = useSelector((state) => state.users);
  const history = useHistory();

  useEffect(() => {
    dispatch(getUsers());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const TableColumns = useMemo(
    () => [
      { title: "#", field: "id_user", maxWidth: "50px", width: "50px" },
      {
        title: "Nombre",
        field: "name",
      },
      {
        title: "Correo",
        field: "email",
      },
      {
        title: "Teléfono",
        field: "phone",
      },
      {
        title: "Tipo de registro",
        field: "register_type",
      },
    ],
    []
  );

  const doAdmin = (rowData) => { 
    dispatch(updateUser({ ...rowData, role_id: 1 }));
  };

  const quiteAdmin = (rowData) => {
    dispatch(updateUser({ ...rowData, role_id: 2 }));
  };

  const TableActions = useMemo(
    () => [
      (rowData) => ({
        icon: "admin_panel_settings",
        onClick: (_event, rowData) => quiteAdmin(rowData),
        tooltip: "Quitar admin",
        hidden: rowData.role_id === 2,
        disable: rowData.role_id === 2,
        iconProps: {
          color: "secondary",
        },
      }),
      (rowData) => ({
        icon: "admin_panel_settings",
        onClick: (_event, rowData) => doAdmin(rowData),
        tooltip: "Hacer admin",
        hidden: rowData.role_id === 1,
        disable: rowData.role_id === 1,
        iconProps: {
          color: "disabled",
        },
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const onRowClick = (_ev, rowData) => {
    history.push(`/usuarios/info/${rowData.id_user}`);
  };

  return (
    <Container>
      <PageHeader title="Usuarios" />
      <TableComponent
        columns={TableColumns}
        data={users}
        actions={TableActions}
        customLocalization={{
          header: {
            actions: "Admin",
          },
        }}
        onRowClick={onRowClick}
      />
      <AppLoader isVisible={loadingUsers} />
    </Container>
  );
}
