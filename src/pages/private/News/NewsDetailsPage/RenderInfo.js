import React, { useState } from "react";
import { Link } from "react-router-dom";
import { PageHeader } from "../../../../assets/styles/Page";
import { MinText, Text } from "../../../../assets/styles/Text";
import AddImageComponent from "../../../../components/AddImage/AddImageComponent";
// import ImagePreview from "../../../components/AddImage/ImagePreview";
import { Button } from "../../../../components/Buttons";

import {
  ColDetail,
  RowDetail,
  SubtitleDetail,
  ColDescription,
} from "../../../../components/grid/DetailsComponents";
import ReadImages from "../../../../components/ReadImages";
import Theme from "../../../../constants/Theme";
// import { AddPhotoNew } from "../../../store/actions/News";

const RenderInfo = ({ info, callBack, onDeleteImage }) => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const onPressDeleteImg = (item) => onDeleteImage(item);

  return (
    <section>
      <PageHeader title={info.title} />
      <RowDetail>
        <ColDetail>
          <SubtitleDetail children="Fecha de publicación: " />
          <Text className="inline" children={info.publication_date} />
        </ColDetail>

        <ColDetail>
          <SubtitleDetail children="Región: " />
          <Text className="inline" children={info.region} />
        </ColDetail>

        <ColDetail>
          <SubtitleDetail children="Me gusta totales: " />
          <Text className="inline" children={info.likes} />
        </ColDetail>
      </RowDetail>
      <br />
      <RowDetail>
        <ColDescription>
          <SubtitleDetail children="Descripción:" />
          <MinText color={Theme.textColor} children={info.description} />
        </ColDescription>
      </RowDetail>
      <br />
      <div className="flex space-between align-center mb-1">
        <SubtitleDetail children="Imágenes:" />
        <Button
          bgColor={Theme.primaryColor}
          onPress={() => {
            setIsOpenModal(true);
          }}
        >
          Agregar imagen
        </Button>
      </div>
      <div className="row">
        <ReadImages images={info.images} deleteAction={onPressDeleteImg} />
      </div>
      <br />
      <br />
      <div style={{ display: "block" }} />
      <div>
        <div className="centerItems ">
          <Button
            children="Editar información"
            as={Link}
            to={`/noticias/editar/${info.id_new}`}
          />
        </div>
      </div>
      <br />
      <AddImageComponent
        isOpen={isOpenModal}
        changeModal={setIsOpenModal}
        dataSend={{
          endPoint: "news/photo/add",
          name: "id_new",
          val: info.id_new,
          nameFile: "image",
        }}
        callBack={callBack}
      />
    </section>
  );
};

export default RenderInfo;
