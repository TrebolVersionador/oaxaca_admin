import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { PageHeader } from "../../../../assets/styles/Page";
import Container from "../../../../components/Container";
import { deleteImageNew, getInfoById } from "../../../../store/actions/News";
import Loader from "../../../../components/Loader";
import RenderInfo from "./RenderInfo";

export default function NewsDetailsPage() {
  const { id_new } = useParams();
  const [info, setInfo] = useState({});
  const [loadingInfo, setLoadingInfo] = useState(true);

  useEffect(() => {
    getInfoNew();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_new]);

  const getInfoNew = async () => {
    let info = await getInfoById(id_new);
    const { data, status } = info;
    console.log(data);
    if (status === 200) {
      setLoadingInfo(false);
      setInfo(data);
    } else {
      setLoadingInfo(false);
    }
  };

  const onDeleteImage = async (item) => {
    if (await deleteImageNew(item)) {
      let nextImages = info.images.filter(
        (img) => img.id_new_photo !== item.id_new_photo
      );
      let nextInfo = { ...info, images: nextImages };
      setInfo(nextInfo);
    }
  };

  return (
    <Container isSecondary>
      {loadingInfo ? (
        <Loader />
      ) : !!info.title ? (
        <RenderInfo
          info={info}
          callBack={getInfoNew}
          onDeleteImage={onDeleteImage}
        />
      ) : (
        <RenderError />
      )}
    </Container>
  );
}

const RenderError = () => {
  return (
    <section>
      <PageHeader title="Error" />
      <p>NO pudimos resuperar los datos</p>
    </section>
  );
};
