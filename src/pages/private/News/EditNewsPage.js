import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { InputStyled, TextAreaStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDescription,
  ColDetailMax,
  ColDetailMin,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import Loader from "../../../components/Loader";
import RegionSelect from "../../../components/Select/RegionSelect";

import { getInfoById, updateNew } from "../../../store/actions/News";

export default function EditNewsPage() {
  const dispatch = useDispatch();
  const formRef = useRef();
  const history = useHistory();
  const { id_new } = useParams();
  const [info, setInfo] = useState({});
  const [isLoading, setLoadingInfo] = useState(true);

  useEffect(() => {
    getInfoNew();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id_new]);

  const getInfoNew = async () => {
    let info = await getInfoById(id_new);
    const { data, status } = info;
    if (status === 200) {
      setLoadingInfo(false);
      setInfo(data);
    } else {
      setLoadingInfo(false);
    }
  };
  /*  */
  const onSubmit = (e) => {
    e.preventDefault();
    const { id_region, title, description, new_url } = e.target;
    if (!id_region.value) return false;
    dispatch(
      updateNew(
        {
          ...info,
          id_region: id_region.value,
          title: title.value,
          description: description.value,
          new_url: new_url.value ?? "",
          region: "Cañada",
        },
        () => {
          history.goBack();
        }
      )
    );
  };

  if (isLoading) return <Loader />;

  return (
    <Container isSecondary>
      <PageHeader title="Editar notica" />
      <form onSubmit={onSubmit} ref={formRef}>
        <RowDetail>
          <ColDescription>
            <InputStyled
              placeholder="Título de la noticia"
              name="title"
              required
              width="100%"
              defaultValue={info.title}
            />
          </ColDescription>
          <ColDetailMax>
            <RegionSelect defaultValue={info.id_region} />
          </ColDetailMax>

          <ColDetailMax>
            <InputStyled
              placeholder="Url de la noticia"
              name="new_url"
              type="url"
              width="100%"
              defaultValue={info.new_url}
            />
          </ColDetailMax>
          <ColDetailMin>
            <InputStyled
              placeholder="Fecha"
              name="publication_date"
              type="date"
              width="100%"
              defaultValue={info.publication_date}
            />
          </ColDetailMin>
          <ColDescription>
            <TextAreaStyled
              placeholder="Cuerpo de la noticia"
              name="description"
              required
              defaultValue={info.description}
            />
          </ColDescription>
        </RowDetail>

        <div className="flex justify-center align-center mt-1 mb-1">
          <Button children="ACTUALIZAR NOTICIA" type="xLarge" />
        </div>
      </form>
    </Container>
  );
}
