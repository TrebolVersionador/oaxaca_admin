import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { PageHeader } from "../../../assets/styles/Page";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import Loader from "../../../components/Loader";
import TableComponent from "../../../components/TableComponent";
import { truncateText } from "../../../constants/Helpers";
import { disableNew, getAllNews } from "../../../store/actions/News";

export default function NewsPage() {
  const { news = [], isLoading } = useSelector((state) => state.news);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllNews());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onPressNew = () => {
    history.push("/noticias/crear");
  };

  const onDeletePress = (rowData) => {
    dispatch(disableNew(rowData, 0));
  };
  const onActivePress = (rowData) => {
    dispatch(disableNew(rowData, 1));
  };

  const TableActions = useMemo(
    () => [
      (rowData) => ({
        icon: "visibility",
        onClick: (_event, rowData) => onDeletePress(rowData),
        hidden: !rowData.active,
        disabled: !rowData.active,
        tooltip: "Ocultar",
        iconProps: {
          color: "secondary",
        },
      }),
      (rowData) => ({
        icon: "visibility_off",
        tooltip: "Mostrar",
        onClick: (_event, rowData) => onActivePress(rowData),
        hidden: !!rowData.active,
        disabled: !!rowData.active,
        iconProps: {
          color: "disabled",
        },
      }),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const onRowClick = (_event, rowData) => {
    history.push(`/noticias/info/${rowData.id_new}`);
  };

  return (
    <Container>
      <PageHeader
        // title required
        title="NOTICIAS"
        // action button
        LeftChild={() => (
          <Button children="Agregar nueva notica" onPress={onPressNew} />
        )}
      />
      {isLoading ? (
        <Loader />
      ) : (
        <TableComponent
          columns={TableColumns}
          data={news}
          actions={TableActions}
          onRowClick={onRowClick}
        />
      )}
    </Container>
  );
}

const TableColumns = [
  { title: "#", field: "id_new", maxWidth: "50px", width: "50px" },
  {
    title: "Título de la notica",
    field: "title",
    render: (rowData) => (
      <span style={{ width: 50, borderRadius: "50%" }}>
        {truncateText(rowData.title)}
      </span>
    ),
  },
  { title: "Fecha de publicación", field: "publication_date" },
  {
    title: "Región",
    field: "region",
  },
  {
    title: "Me gusta",
    field: "likes",
  },
];
/*

active: 1
description: "El director general, Francisco Ángel Villarreal informó que a través de este esquema se garantizan los servicios educativos y la atención a 26 mil 450 niñas y niños de 318 comunidades Oaxaca de Juárez, Oax. 12 de noviembre de 2020.- Resultado de las gestiones del gobernador Alejandro Murat Hinojosa y el decidido apoyo del presidente de México, Andrés Manuel López Obrador, en el año 2021 continuará en Oaxaca la Estrategia Nacional de Iniciación a la Docencia en el Medio Indígena, proyecto único en el país, informó el director general del Instituto Estatal de Educación Pública de Oaxaca (IEEPO), Francisco Ángel Villarreal.  Explicó que a través de este mecanismo, ejemplo a seguir a nivel nacional, con el apoyo de 400 jóvenes bachilleres hablantes de lenguas originarias y sus variantes dialectales, nativos de localidades necesitadas de profesores en escuelas de los niveles de educación inicial, preescolar y primaria, quienes fueron seleccionados por el IEEPO, el magisterio, padres, madres de familia y autoridades municipales, en una primera fase se atendieron a 26 mil 450 niñas y niños de 318 comunidades de diferentes regiones.  El Director General del IEEPO resaltó que en la reciente mesa nacional de atención al magisterio, encabezada por el presidente Andrés Manuel López Obrador y el secretario de Educación Pública, Esteban Moctezuma Barragán, en Palacio Nacional, se ratificó seguir con la Estrategia, para lo cual se autorizó el presupuesto que se requiere en su ejecución.  La prioridad es contribuir en la revitalización de las lenguas originarias y sus variantes dialectales desde el trabajo del aula y promover estrategias de inclusión, capacitación y sensibilización para preservar el legado y riqueza de los pueblos originarios, enfatizó al refrendar el compromiso de la institución con la equidad y pertinencia en la prestación de servicios educativos.  En entrevista, Ángel Villarreal indicó que la Secretaría de Educación Pública (SEP), el Gobierno del Estado y el magisterio oaxaqueño acordaron, a finales de 2019, la Estrategia Nacional de Iniciación a la Docencia en el Medio Indígena para la formación de futuros maestros en este nivel y garantizar el acceso a la prestación de servicios educativos en las distintas comunidades y pueblos originarios de la entidad, lo cual ha sido una demanda de varios años.  Los 400 jóvenes bachilleres, tras una valoración académica de ingreso, fueron capacitados para impartir los niveles de educación inicial, preescolar y primaria con el apoyo de una beca mensual."
id_new: 45
id_region: 1
image: "http://api.gobiernoinforma.com/news/newnew.jpg"
likes: 6
new_url: "https://www.oaxaca.gob.mx/comunicacion/en-2021-continuara-en-oaxaca-la-estrategia-de-iniciacion-a-la-docencia-en-el-medio-indigena-ieepo/"
publication_date: "2020-11-12"
region: "Cañada"
title: "En 2021 continuar"

*/
