import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { InputStyled, TextAreaStyled } from "../../../assets/styles/Forms";
import { PageHeader } from "../../../assets/styles/Page";
import AddImageForm from "../../../components/AddImage/AddImageForm";
import { Button } from "../../../components/Buttons";
import Container from "../../../components/Container";
import {
  ColDescription,
  ColDetail,
  ColDetailMax,
  ColDetailMin,
  RowDetail,
} from "../../../components/grid/DetailsComponents";
import RegionSelect from "../../../components/Select/RegionSelect";
import { addNew } from "../../../store/actions/News";

export default function AddNoticePage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const onSubmit = (e) => {
    e.preventDefault();
    const {
      id_region,
      title,
      description,
      new_url,
      img_one,
      publication_date,
    } = e.target;
    if (!id_region.value) return false;

    dispatch(
      addNew(
        {
          title: title.value,
          new_url: new_url.value ?? "",
          region: "Cañada",
          id_region: id_region?.value,
          publication_date: publication_date?.value,
          description: description?.value,
        },
        img_one.files[0],
        (id_new) => {
          history.replace(`/noticias/info/${id_new}`);
        }
      )
    );
  };

  return (
    <Container isSecondary>
      <PageHeader title="Nueva notica" />
      <form onSubmit={onSubmit}>
        <RowDetail>
          <ColDescription>
            <InputStyled
              placeholder="Título de la noticia"
              name="title"
              required
              width="100%"
            />
          </ColDescription>
          <ColDetail>
            <RegionSelect defaultValue={0} />
          </ColDetail>
          <ColDetailMax>
            <InputStyled
              placeholder="Url de la noticia"
              name="new_url"
              type="url"
              width="100%"
            />
          </ColDetailMax>
          <ColDetailMin>
            <InputStyled
              placeholder="Fecha"
              name="publication_date"
              type="date"
              width="100%"
            />
          </ColDetailMin>

          <ColDescription>
            <TextAreaStyled
              placeholder="Cuerpo de la noticia"
              name="description"
              required
            />
          </ColDescription>
        </RowDetail>
        <AddImageForm name="img_one" required />
        <div className="flex justify-center align-center mt-1">
          <Button children="CREAR NOTICIA" type="xLarge" />
        </div>
      </form>
    </Container>
  );
}
