import React from "react";
import styled from "styled-components";

export const DeleteX = ({ action }) => (
  <DeleteImg onClick={action}>
    <i className="material-icons">close</i>
  </DeleteImg>
);

const DeleteImg = styled.button`
  color: rgba(219, 34, 18, 0.3);
  position: absolute;
  top: 0.5em;
  right: 0.5em;

  &:hover {
    color: rgba(219, 34, 18, 2);
    background-color: rgba(250, 250, 250, 0.2);
  }
`;
