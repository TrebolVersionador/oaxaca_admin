import styled from "styled-components";

export const Text = styled.p`
  margin: 0;
  font-size: 1em;
  color: ${({ color, theme }) => color ?? theme.textColor};
  font-weight: ${({ weight }) => (weight ? weight : "normal")};
`;

export const Title = styled.p`
  margin: 0;
  font-size: 1.5em;
  font-weight: ${({ weight }) => (weight ? weight : "900")};
  color: ${({ color, theme }) => color ?? theme.primaryColor};
`;

export const Subtitle = styled.p`
  margin: 0;
  font-size: 1.4em;
  font-weight: ${({ weight }) => (weight ? weight : "bold")};
  color: ${({ color, theme }) => color ?? theme.secondaryColor};
`;

export const MinText = styled.p`
  margin: 0;
  font-size: 0.8em;
  color: ${({ color, theme }) => color ?? theme.grey};
  font-weight: ${({ weight }) => weight ?? "200"};
`;

export const LinkText = styled.a`
  margin: 0;
  font-size: 0.8em;
  color: ${({ color, theme }) => color ?? theme.grey};
  font-weight: ${({ weight }) => (weight ? weight : "normal")};
  text-decoration: none;
`;
