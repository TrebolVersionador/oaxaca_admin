import styled from "styled-components";

export const InputStyled = styled.input`
  padding: 0.5em;
  border: ${({ theme }) => theme.borderInput};
  font-size: 1em;
  margin: 0.5em 0;
  width: ${({ width }) => width ?? "40%"};

  &::placeholder {
    color: grey;
  }

  @media screen and (max-width: 600px) {
    width: 100%;
  }
`;

export const SelectStyled = styled.select`
  padding: 0.5em;
  border: ${({ theme }) => theme.borderInput};
  font-size: 1em;
  width: 100%;
  margin: 0.5em 0;

  &::placeholder {
    color: grey;
  }
`;

export const TextAreaStyled = styled.textarea`
  padding: 0.5em;
  border: ${({ theme }) => theme.borderInput};
  font-size: 1em;
  width: calc(100% - 1em);
  min-height: 240px;
  margin: 0.5em 0;

  &::placeholder {
    color: grey;
  }
`;

export const LabelAddImage = styled.label`
  width: ${({ width }) => width ?? "250px"};
  height: 180px;
  border: ${({ theme }) => theme.borderInput};
  background-color: lightgray;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  position: relative;
  margin: 0.5em 0;

  & input {
    width: 1px;
    height: 1px;
    overflow: hidden;
    opacity: 0;
  }

  & div {
    text-align: center;
    display: inline-block;
  }

  & img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    top: 0;
    left: 0;
  }

  & .floatBtn {
    position: absolute;
    bottom: 0;
  }

  & .floatDelete {
    position: absolute;
    top: 0;
    right: 0;
  }
`;
