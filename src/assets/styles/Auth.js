import styled from "styled-components";

export const Page = styled.main`
  background-color: ${({ theme }) => theme.canvas};
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 500px;
`;

export const OtherOption = styled.div`
  padding: 1em;
  border-top: 1px solid #bab6b6;
  margin-top: 1em;
  text-align: center;

  & .social-options {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 1em;

    & button:nth-child(2) {
      margin: 0 1em;
    }
  }
`;

export const BodyCard = styled.div`
  /* grid-area: 3 / 1 / -1 / 6; */
  border-bottom-left-radius: 1em;
  border-bottom-right-radius: 1em;
  border: 2px solid ${({ theme }) => theme.grey};
  border-top: none;
  background-color: white;
  padding: 1em 2em;

  & form {
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;

export const HeaderCard = styled.div`
  /* grid-area: 1 / 1 / 3 / 6; */
  background-color: ${({ theme }) => theme.primaryColor};
  border: 2px solid ${({ theme }) => theme.grey};
  border-bottom: none;
  border-top-left-radius: 1em;
  border-top-right-radius: 1em;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1em;
  /* height: 200px; */

  & img {
    height: 50px;
  }
`;

export const Card = styled.section`
  width: 55%;
  max-width: 350px;
  /* height: 50%; */
  min-height: 270px;

  /* display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(10, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px; */

  @media screen and (max-width: 400px) {
    width: 85%;
  }
  @media screen and (max-width: 500px) {
    width: 80%;
  }
`;
