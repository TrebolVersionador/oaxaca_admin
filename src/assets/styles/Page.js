import PropTypes from "prop-types";
import { Title } from "./Text";
import styled from "styled-components";

export const PageHeader = ({ title, LeftChild }) => {
  return (
    <Header>
      <Title>{title}</Title>
      <div>{!!LeftChild && <LeftChild />}</div>
    </Header>
  );
};

PageHeader.propTypes = {
  LeftChild: PropTypes.elementType,
  title: PropTypes.any.isRequired,
};

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1em;
`;
