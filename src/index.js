import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./assets/styles/normalize.css";
import "./assets/styles/style.css";
import "./assets/styles/grid.css";
import Axios from "axios";
import { apiUrl } from "./constants/constants";

Axios.defaults.baseURL = apiUrl;

Axios.interceptors.request.use(function (config) {
  var token = document.cookie.replace(
    /(?:(?:^|.*;\s*)token\s*=\s*([^;]*).*$)|^.*$/,
    "$1"
  );

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

Axios.interceptors.response.use(
  function (response) {
    // clean response
    return {
      data: response.data?.data ?? {},
      status: response.status,
      message: response.data?.message ?? "",
    };
  },
  function (error) {
    return Promise.reject(error);
  }
);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
