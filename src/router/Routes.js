import PrivateRoutes from "../pages/private";
import PublicRoutes from "../pages/public";

const Routes = [...PrivateRoutes, ...PublicRoutes];


export default Routes;
