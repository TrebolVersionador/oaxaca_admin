import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";
import Routes from "./Routes";
import { getAuthStatus } from "../store/actions/Auth";

export default function Router() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAuthStatus());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Switch>
      {Routes.map((itemRoute, index) => {
        return (
          <Route
            key={`route-${itemRoute.name}-${index}`}
            path={itemRoute.path}
            exact={true}
            component={itemRoute.component}
          />
        );
      })}
      <Redirect exact="true" path="/" to="/noticias" />
    </Switch>
  );
}

/* const PrivateRoute = (itemRoute) => {
  const isLoggin = true;
  if (!isLoggin) return <Redirect to="/iniciar-sesion" />;
  return (
    <AppContainer>
      <Route path={itemRoute.path} component={itemRoute.component} />;
    </AppContainer>
  );
};
 */
