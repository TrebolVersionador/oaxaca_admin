import {
  GET_AUTH_STATUS,
  GET_AUTH_STATUS_SUCCESS,
  GET_AUTH_STATUS_ERROR,
  GET_USER_DATA,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_ERROR,
  LOGGING,
  LOGGING_ERROR,
  LOGGING_SUCCESS,
  LOGOUT,
} from "../types/Auth";

const initialState = {
  isLoadingApp: true,
  isLoggin: false,
  userData: null, // {}
  token: null,
};

function AuthReducer(state = initialState, action) {
  switch (action.type) {
    case GET_AUTH_STATUS:
      return { ...state, isLoadingApp: true };
    case GET_AUTH_STATUS_SUCCESS:
      return {
        ...state,
        isLoadingApp: false,
        isLoggin: true,
        token: action.payload.token,
        idUser: action.payload.id_user,
      };
    case GET_AUTH_STATUS_ERROR:
      return {
        ...state,
        isLoadingApp: false,
        isLoggin: false,
        messsage: action.payload,
      };
    // Try loggin
    case LOGGING:
      return { ...state, isLoading: true };
    case LOGGING_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLoggin: true,
        token: action.payload.token,
        idUser: action.payload.id_user,
        messageError: "",
      };
    case LOGGING_ERROR:
      return { ...state, isLoggin: false, messageError: action.payload };
    // GET USER DATA
    case GET_USER_DATA:
      return { ...state, isLoading: true };
    case GET_USER_DATA_SUCCESS:
      return {
        ...state,
        isLoading: false,
        userData: { ...action.payload },
      };
    case GET_USER_DATA_ERROR:
      return { ...initialState };
    case LOGOUT:
      return {
        ...state,
        isLoading: false,
        userData: null,
        isLoggin: false,
        token: null,
      };
    default:
      return state;
  }
}

export default AuthReducer;
