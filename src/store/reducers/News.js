import {
  GET_NEWS,
  GET_NEWS_SUCCESS,
  GET_NEWS_ERROR,
  PUT_NEW,
  PUT_NEW_ERROR,
  PUT_NEW_SUCCESS,
  UPDATE_NEW,
  UPDATE_NEW_SUCCESS,
  UPDATE_NEW_ERROR,
  GET_INFO,
} from "../types/News";

const initialData = {
  news: [],
  isLoading: false,
};

function NewsReducer(state = initialData, action) {
  switch (action.type) {
    case GET_NEWS:
      return { ...state, isLoading: true };
    case GET_NEWS_SUCCESS:
      return { ...state, isLoading: false, news: action.payload };
    case GET_NEWS_ERROR:
      return { ...state, isLoading: false, message: action.payload };
    case PUT_NEW:
      return { ...state, updating: true };
    case PUT_NEW_SUCCESS:
      return { ...state, updating: false, news: action.payload };
    case PUT_NEW_ERROR:
      return { ...state, updating: false, message: action.payload };
    case UPDATE_NEW:
      return { ...state, updating: true };
    case UPDATE_NEW_SUCCESS:
      return { ...state, updating: false, news: action.payload };
    case UPDATE_NEW_ERROR:
      return { ...state, updating: false, message: action.payload };
    case GET_INFO:
      return { ...state, isLoading: true };
    default:
      return { ...state };
  }
}

export default NewsReducer;
