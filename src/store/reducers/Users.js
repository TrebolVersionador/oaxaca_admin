import {
  GET_ALL_USERS,
  GET_ALL_USERS_ERROR,
  GET_ALL_USERS_SUCCESS,
  PUT_USER,
  PUT_USER_SUCCESS,
  PUT_USER_ERROR,
  UPDATE_USER,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,
} from "../types/Users";

const initialState = {
  loadingUsers: false,
  users: [],
};

function UserReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_USERS:
      return { ...state, loadingUsers: true };
    case GET_ALL_USERS_SUCCESS:
      return { ...state, loadingUsers: false, users: action.payload };
    case GET_ALL_USERS_ERROR:
      return { ...state, loadingUsers: false };
    // ADD
    case PUT_USER:
      return { ...state, uploadingUsers: true };
    case PUT_USER_SUCCESS:
      return { ...state, uploadingUsers: false, users: action.payload };
    case PUT_USER_ERROR:
      return { ...state, uploadingUsers: false };
    // UPDATE
    case UPDATE_USER:
      return { ...state, uploadingUsers: true };
    case UPDATE_USER_SUCCESS:
      return { ...state, uploadingUsers: false, users: action.payload };
    case UPDATE_USER_ERROR:
      return { ...state, uploadingUsers: false };
    default:
      return { ...state };
  }
}

export default UserReducer;
