import { combineReducers } from "redux";
import AuthReducer from "./Auth";
import CatalogueReducer from "./Catalogue";
import CommitmentsReducer from "./Commitments";
import DirectoryReducer from "./Directory";
import NewsReducer from "./News";
import SpecialTopicsReducer from "./SpecialTopics";
import VideosReducer from "./Video";
import StaticalAnnexReducer from "./StaticalAnnex";
import UserReducer from "./Users";

const Reducer = combineReducers({
  auth: AuthReducer,
  news: NewsReducer,
  commitments: CommitmentsReducer,
  videos: VideosReducer,
  directory: DirectoryReducer,
  topics: SpecialTopicsReducer,
  catalogue: CatalogueReducer,
  axles: StaticalAnnexReducer,
  users: UserReducer,
});

export default Reducer;
