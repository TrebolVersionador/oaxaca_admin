import {
  GET_OFFICERS,
  GET_OFFICERS_ERROR,
  GET_OFFICERS_SUCCESS,
  PUT_OFFICER,
  PUT_OFFICER_ERROR,
  PUT_OFFICER_SUCCESS,
} from "../types/Directory";

const initialState = {
  isLoading: true,
  officers: [],
};

function DirectoryReducer(state = initialState, action) {
  switch (action.type) {
    case GET_OFFICERS:
      return { ...state, isLoading: true };
    case GET_OFFICERS_SUCCESS:
      return { ...state, isLoading: false, officers: action.payload };
    case GET_OFFICERS_ERROR:
      return { ...state, isLoading: false };
    case PUT_OFFICER:
      return { ...state, updating: true };
    case PUT_OFFICER_SUCCESS:
      return { ...state, updating: false, officers: action.payload };
    case PUT_OFFICER_ERROR:
      return { ...state, updating: false };
    default:
      return { ...state };
  }
}

export default DirectoryReducer;
