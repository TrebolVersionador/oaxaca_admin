import {
  GET_STATICAL_ANNEX,
  GET_STATICAL_ANNEX_ERROR,
  GET_STATICAL_ANNEX_SUCCESS,
  PUT_STATICAL_ANNEX,
  PUT_STATICAL_ANNEX_SUCCESS,
  PUT_STATICAL_ANNEX_ERROR,
  UPDATE_STATICAL_ANNEX,
  UPDATE_STATICAL_ANNEX_SUCCESS,
  UPDATE_STATICAL_ANNEX_ERROR,
  GET_AXLES,
  GET_AXLES_SUCCESS,
  GET_AXLES_ERROR,
  PUT_AXLES,
  PUT_AXLES_SUCCESS,
  PUT_AXLES_ERROR,
  UPDATE_AXLES,
  UPDATE_AXLES_SUCCESS,
  UPDATE_AXLES_ERROR,
} from "../types/StaticalAnnex";

const initialState = {
  isLoading: false,
  staticalAnnex: [],
  axlesList: [],
};

function StaticalAnnexReducer(state = initialState, action) {
  switch (action.type) {
    case GET_STATICAL_ANNEX:
      return { ...state, loadingStatical: true };
    case GET_STATICAL_ANNEX_SUCCESS:
      return {
        ...state,
        loadingStatical: false,
        staticalAnnex: action.payload,
      };
    case GET_STATICAL_ANNEX_ERROR:
      return {
        ...state,
        loadingStatical: false,
      };
    // ADD STATICAL
    case PUT_STATICAL_ANNEX:
      return { ...state, loadingStatical: true };
    case PUT_STATICAL_ANNEX_SUCCESS:
      return {
        ...state,
        loadingStatical: false,
        staticalAnnex: action.payload,
      };
    case PUT_STATICAL_ANNEX_ERROR:
      return {
        ...state,
        loadingStatical: false,
      };
    // UPDATE
    case UPDATE_STATICAL_ANNEX:
      return { ...state, loadingStatical: true };
    case UPDATE_STATICAL_ANNEX_SUCCESS:
      return {
        ...state,
        loadingStatical: false,
        staticalAnnex: action.payload,
      };
    case UPDATE_STATICAL_ANNEX_ERROR:
      return {
        ...state,
        loadingStatical: false,
      };

    // AXLES
    case GET_AXLES:
      return { ...state, loadingAxles: true };
    case GET_AXLES_SUCCESS:
      return {
        ...state,
        loadingAxles: false,
        axlesList: action.payload,
      };
    case GET_AXLES_ERROR:
      return {
        ...state,
        loadingAxles: false,
      };
    // ADD STATICAL
    case PUT_AXLES:
      return { ...state, loadingAxles: true };
    case PUT_AXLES_SUCCESS:
      return {
        ...state,
        loadingAxles: false,
        axlesList: action.payload,
      };
    case PUT_AXLES_ERROR:
      return {
        ...state,
        loadingAxles: false,
      };
    // UPDATE
    case UPDATE_AXLES:
      return { ...state, loadingAxles: true };
    case UPDATE_AXLES_SUCCESS:
      return {
        ...state,
        loadingAxles: false,
        axlesList: action.payload,
      };
    case UPDATE_AXLES_ERROR:
      return {
        ...state,
        loadingAxles: false,
      };

    default:
      return { ...state };
  }
}

export default StaticalAnnexReducer;
