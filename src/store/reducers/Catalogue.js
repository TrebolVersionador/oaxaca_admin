import {
  GET_AREAS,
  GET_AREAS_ERROR,
  GET_AREAS_SUCCESS,
  GET_LOCATIONS,
  GET_LOCATIONS_ERROR,
  GET_LOCATIONS_SUCCESS,
  GET_MUNICIPIOS,
  GET_MUNICIPIOS_ERROR,
  GET_MUNICIPIOS_SUCCESS,
  GET_REGIONS,
  GET_REGIONS_ERROR,
  GET_REGIONS_SUCCESS,
  GET_TITLES,
  GET_TITLES_ERROR,
  GET_TITLES_SUCCESS,
} from "../types/Catalogues";

const inititalState = {
  locations: [],
  loadingLocations: false,
  titles: [],
  loadingTitles: true,
  municipios: [],
  loadingMunicipios: true,
  regions: [],
  loadingRegions: true,
  areas: [],
  loadingAreas: true,
};

function CatalogueReducer(state = inititalState, action) {
  switch (action.type) {
    case GET_AREAS:
      return { ...state, loadingAreas: true };
    case GET_AREAS_SUCCESS:
      return { ...state, loadingAreas: false, areas: action.payload };
    case GET_AREAS_ERROR:
      return { ...state, loadingAreas: false };
    case GET_LOCATIONS:
      return { ...state, loadingLocations: true };
    case GET_LOCATIONS_SUCCESS:
      return { ...state, loadingLocations: false, locations: action.payload };
    case GET_LOCATIONS_ERROR:
      return { ...state, loadingLocations: false };
    case GET_MUNICIPIOS:
      return { ...state, loadingMunicipios: true };
    case GET_MUNICIPIOS_SUCCESS:
      return { ...state, loadingMunicipios: false, municipios: action.payload };
    case GET_MUNICIPIOS_ERROR:
      return { ...state, loadingMunicipios: false };
    case GET_REGIONS:
      return { ...state, loadingRegions: true };
    case GET_REGIONS_SUCCESS:
      return { ...state, loadingRegions: false, regions: action.payload };
    case GET_REGIONS_ERROR:
      return { ...state, loadingRegions: false };
    case GET_TITLES:
      return { ...state, loadingTitles: true };
    case GET_TITLES_SUCCESS:
      return { ...state, loadingTitles: false, titles: action.payload };
    case GET_TITLES_ERROR:
      return { ...state, loadingTitles: false };
    default:
      return { ...state };
  }
}
export default CatalogueReducer;
