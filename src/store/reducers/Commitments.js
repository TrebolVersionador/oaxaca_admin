import {
  GET_COMMITMENTS,
  GET_COMMITMENTS_ERROR,
  GET_COMMITMENTS_SUCCESS,
  PUT_COMMITMENT,
  PUT_COMMITMENT_ERROR,
  PUT_COMMITMENT_SUCCESS,
} from "../types/Commitments";

let initialState = {
  isLoading: false,
  commitments: [],
};

function CommitmentsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMMITMENTS:
      return { ...state, isLoading: true };
    case GET_COMMITMENTS_SUCCESS:
      return { ...state, isLoading: false, commitments: action.payload };
    case GET_COMMITMENTS_ERROR:
      return { ...state, isLoading: false };
    case PUT_COMMITMENT:
      return { ...state, uploading: true };
    case PUT_COMMITMENT_SUCCESS:
      return { ...state, uploading: false, commitments: action.payload };
    case PUT_COMMITMENT_ERROR:
      return { ...state, uploading: false };
    default:
      return { ...state };
  }
}

export default CommitmentsReducer;
