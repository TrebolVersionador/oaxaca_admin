import {
  GET_STREAMS,
  GET_STREAMS_ERROR,
  GET_STREAMS_SUCCESS,
  GET_VIDEOS,
  GET_VIDEOS_ERROR,
  GET_VIDEOS_SUCCESS,
  PUT_STREAM,
  PUT_STREAM_ERROR,
  PUT_STREAM_SUCCESS,
  PUT_VIDEO,
  PUT_VIDEO_ERROR,
  PUT_VIDEO_SUCCESS,
  UPDATE_MEDIA,
  UPDATE_MEDIA_RESPONSE,
  UPDATE_STREAM,
  UPDATE_STREAM_ERROR,
  UPDATE_STREAM_SUCCESS,
} from "../types/Video";

const initialState = {
  isLoading: true,
  isUpdating: false,
  videosList: [],
  streamsVideos: [],
};

function VideosReducer(state = initialState, action) {
  switch (action.type) {
    case GET_VIDEOS:
      return { ...state, isLoading: true };
    case GET_VIDEOS_SUCCESS:
      return { ...state, isLoading: false, videosList: action.payload };
    case GET_VIDEOS_ERROR:
      return { ...state, isLoading: false, videosList: [] };
    case PUT_VIDEO:
      return { ...state, isLoading: true };
    case PUT_VIDEO_SUCCESS:
      return { ...state, isLoading: false, videosList: action.payload };
    case PUT_VIDEO_ERROR:
      return { ...state, isLoading: false };
    case UPDATE_MEDIA:
      return { ...state, updating: true };
    case UPDATE_MEDIA_RESPONSE:
      return { ...state, updating: false };
    // STREAMS
    case GET_STREAMS:
      return { ...state, loadingStreams: false, streamsList: action.payload };
    case GET_STREAMS_SUCCESS:
      return { ...state, loadingStreams: false, streamsList: action.payload };
    case GET_STREAMS_ERROR:
      return { ...state, loadingStreams: false, streamsList: [] };
    // ADD
    case PUT_STREAM:
      return { ...state, uploadingStreams: true };
    case PUT_STREAM_SUCCESS:
      return { ...state, uploadingStreams: false, streamsList: action.payload };
    case PUT_STREAM_ERROR:
      return { ...state, uploadingStreams: false };
    // EDIT DELTE
    case UPDATE_STREAM:
      return { ...state, uploadingStreams: true };
    case UPDATE_STREAM_SUCCESS:
      return { ...state, uploadingStreams: false, streamsList: action.payload };
    case UPDATE_STREAM_ERROR:
      return { ...state, uploadingStreams: false };
    default:
      return { ...state };
  }
}

export default VideosReducer;
