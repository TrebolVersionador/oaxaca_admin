import {
  GET_TOPICS,
  GET_TOPICS_ERROR,
  GET_TOPICS_SUCCESS,
  PUT_TOPICS,
  PUT_TOPICS_ERROR,
  PUT_TOPICS_SUCCESS,
} from "../types/SpecialTopics";

const initialState = {
  topics: [],
  isLoading: false,
  uploading: false,
};

function SpecialTopicsReducer(state = initialState, action) {
  switch (action.type) {
    case GET_TOPICS:
      return { ...state, isLoading: true };
    case GET_TOPICS_SUCCESS:
      return { ...state, isLoading: false, topics: action.payload };
    case GET_TOPICS_ERROR:
      return { ...state, isLoading: false };
    case PUT_TOPICS:
      return { ...state, uploading: true };
    case PUT_TOPICS_SUCCESS:
      return { ...state, uploading: false, topics: action.payload };
    case PUT_TOPICS_ERROR:
      return { ...state, uploading: false };
    default:
      return { ...state };
  }
}

export default SpecialTopicsReducer;
