import Axios from "axios";
import moment from "moment";
import {
  GET_NEWS,
  GET_NEWS_SUCCESS,
  GET_NEWS_ERROR,
  PUT_NEW,
  PUT_NEW_SUCCESS,
  PUT_NEW_ERROR,
} from "../types/News";

export const getAllNews = (force = false) => (dispatch, getState) => {
  const { news } = getState().news;
  if (!force && news.length > 0) {
    return dispatch({
      type: GET_NEWS_ERROR,
      payload: "",
    });
  } // prevent

  dispatch({ type: GET_NEWS });

  Axios.get(`/news/list`)
    .then(({ status, data }) => {
      if (status !== 200) {
        throw new Error();
      } else {
        dispatch({
          type: GET_NEWS_SUCCESS,
          payload: data.reverse(),
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_NEWS_ERROR,
        payload: "",
      });
    });
};

export const disableNew = (newData, newVal) => (dispatch, getState) => {
  dispatch({
    type: PUT_NEW,
  });
  const { news = [] } = getState().news;

  Axios.post(`news/update/${newData.id_new}`, {
    ...newData,
    active: newVal,
    status: newVal ? "active" : "disabled",
  });

  const updatedNews = news.map((item) => {
    if (item.id_new === newData.id_new) {
      return {
        ...item,
        active: newVal,
        status: newVal ? "active" : "disabled",
      };
    }
    return item;
  });

  dispatch({
    type: PUT_NEW_SUCCESS,
    payload: [...updatedNews],
  });
};

export const addNew = (info, photo, callback) => (dispatch, getState) => {
  const { news } = getState().news;

  dispatch({
    type: PUT_NEW,
  });
  const currentDate = moment(info.publication_date);

  let dataNew = {
    ...info,
    publication_date: currentDate.format("YYYY-MM-DD"),
    status: "active",
  };

  Axios.post("news/add", dataNew)
    .then(({ data, status }) => {
      if (status === 200) {
        let nextNotices = [
          ...news,
          { ...dataNew, id_new: data.id_new, region: info.region },
        ];
        AddPhotoNew(data.id_new, photo)((dispatch, getState));
        dispatch({
          type: PUT_NEW_SUCCESS,
          payload: nextNotices,
        });
        return callback(data.id_new);
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_NEW_ERROR,
      });
    });
};

export const updateNew = (info, callback) => (dispatch, getState) => {
  const { news } = getState().news;

  dispatch({
    type: PUT_NEW,
  });

  Axios.post(`news/update/${info.id_new}`, info)
    .then(({ data, status }) => {
      if (status === 200) {
        let nextNotices = news.map((item) => {
          if (item.id_new === info.id_new) {
            return {
              ...info,
            };
          }
          return item;
        });
        dispatch({
          type: PUT_NEW_SUCCESS,
          payload: nextNotices,
        });
        return callback(data.id_new);
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_NEW_ERROR,
      });
    });
};

export const getInfoById = async (id_new) => {
  return await Axios.get(`news/info/${id_new}`);
};

export const AddPhotoNew = (id_new, photo) => (_dispatch, _getState) => {
  var form = new FormData();
  form.append("image", photo);
  form.append("id_new", id_new);
  Axios.post(`news/photo/add`, form);
};

export const deleteImageNew = async (item) => {
  try {
    const { status } = await Axios.get(
      `news/photo/delete/${item.id_new_photo}`
    );
    return status === 200;
  } catch (e) {
    return false;
  }
};
