import Axios from "axios";
import { apiUrl } from "../../constants/constants";
import {
  GET_COMMITMENTS,
  GET_COMMITMENTS_ERROR,
  GET_COMMITMENTS_SUCCESS,
  PUT_COMMITMENT,
  PUT_COMMITMENT_ERROR,
  PUT_COMMITMENT_SUCCESS,
} from "../types/Commitments";

export const getAllCommitments = (force = false) => (dispatch, getState) => {
  const { commitments } = getState().commitments;
  if (!force && commitments.length > 0) return false;
  dispatch({
    type: GET_COMMITMENTS,
  });

  Axios.get(`${apiUrl}commitments/list`, { filter: "principal" })
    .then(({ data, status }) => {
      console.log(data);
      if (status === 200) {
        return dispatch({
          type: GET_COMMITMENTS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_COMMITMENTS_ERROR,
        payload: "Error",
      });
    });
};

export const disableCommitment = (id_commitment, newVal) => (
  dispatch,
  getState
) => {
  const { commitments } = getState().commitments;

  Axios.post(`${apiUrl}commitments/disable`, { id_commitment }).then(
    ({ status }) => {
      if (status === 200) {
        let newCommitments = commitments.map((item) => {
          if (item.id_commitment === id_commitment) {
            return { ...item, active: 0 };
          }
          return item;
        });
        dispatch({
          type: GET_COMMITMENTS_SUCCESS,
          payload: newCommitments,
        });
      }
    }
  );
};

export const activeCommitment = (id_commitment) => (dispatch, getState) => {
  const { commitments } = getState().commitments;

  Axios.post(`${apiUrl}commitments/disable`, { id_commitment }).then(
    ({ status, data }) => {
      if (status === 200) {
        let newCommitments = commitments.map((item) => {
          if (item.id_commitment === id_commitment) {
            return { ...item, active: 1 };
          }
          return item;
        });
        dispatch({
          type: GET_COMMITMENTS_SUCCESS,
          payload: newCommitments,
        });
      }
    }
  );
};

export const getCommitmentById = (id_commitment) => {
  return Axios.get(`${apiUrl}commitments/consult/${id_commitment}`);
};

export const deleteImageCommitment = async (item) => {
  try {
    const { status } = await Axios.post(`${apiUrl}commitments/delete_image`, {
      id_commitment_image: item.id_commitment_image,
    });
    return status === 200;
  } catch (e) {
    return false;
  }
};

export const addCommitment = (commitment, images, callback) => (
  dispatch,
  getState
) => {
  dispatch({
    type: PUT_COMMITMENT,
  });
  Axios.post("commitments/add", { ...commitment })
    .then(({ data, status }) => {
      if (status === 200) {
        addImageCommitment(images, data.id_commitment, () => {
          getAllCommitments(true)(dispatch, getState);
          callback(data.id_commitment);
        });
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_COMMITMENT_ERROR,
      });
    });
};

export const toggleActiveCommitment = (newVal) => (dispatch, getState) => {
  dispatch({
    type: PUT_COMMITMENT,
  });
  const { commitments } = getState().commitments;

  Axios.post("commitments/update", { ...newVal })
    .then(({ status }) => {
      if (status === 200) {
        let nextCommitments = commitments.map((item) => {
          if (item.id_commitment === newVal.id_commitment) {
            return newVal;
          }
          return item;
        });
        dispatch({
          type: PUT_COMMITMENT_SUCCESS,
          payload: nextCommitments,
        });
        // getAllCommitments(true)(dispatch, getState);
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_COMMITMENT_ERROR,
      });
    });
};

export const editCommitment = (commitment, callback = () => {}) => (
  dispatch,
  getState
) => {
  dispatch({
    type: PUT_COMMITMENT,
  });

  Axios.post("commitments/update", { ...commitment })
    .then(({ status }) => {
      if (status === 200) {
        getAllCommitments(true)(dispatch, getState);
        callback();
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_COMMITMENT_ERROR,
      });
    });
};

export const addImageCommitment = (images, id_commitment, callback) => {
  let services = [];

  Object.keys(images).map((x) => {
    let formData = new FormData();
    formData.append("id_commitment", id_commitment);
    formData.append("url", images[x]);
    services.push(Axios.post(`commitments/add_image`, formData));
    callback();
    return "";
  });

  Axios.all(services);
};
