import Axios from "axios";
import {
  GET_AXLES,
  GET_AXLES_ERROR,
  GET_AXLES_SUCCESS,
  GET_STATICAL_ANNEX,
  GET_STATICAL_ANNEX_ERROR,
  GET_STATICAL_ANNEX_SUCCESS,
  PUT_AXLES,
  PUT_AXLES_ERROR,
  PUT_AXLES_SUCCESS,
  PUT_STATICAL_ANNEX,
  PUT_STATICAL_ANNEX_ERROR,
  PUT_STATICAL_ANNEX_SUCCESS,
  UPDATE_AXLES,
  UPDATE_AXLES_ERROR,
  UPDATE_AXLES_SUCCESS,
  UPDATE_STATICAL_ANNEX,
  UPDATE_STATICAL_ANNEX_ERROR,
  UPDATE_STATICAL_ANNEX_SUCCESS,
} from "../types/StaticalAnnex";

export const getAllStaticalAnnex = (force = false) => (dispatch, getState) => {
  const { staticalAnnex = [] } = getState().axles;
  if (!force && staticalAnnex.length > 0) return false;

  dispatch({
    type: GET_STATICAL_ANNEX,
  });

  Axios.get(`technical_annex/list`)
    .then(({ data, status }) => {
      if (status !== 200) throw new Error();
      dispatch({
        type: GET_STATICAL_ANNEX_SUCCESS,
        payload: data.sort((a, b) => a.page - b.page),
      });
    })
    .catch(() => {
      dispatch({
        type: GET_STATICAL_ANNEX_ERROR,
      });
    });
};

export const addStaticalAnnex = (newData) => (dispatch, getState) => {
  const { staticalAnnex = [] } = getState().axles;

  dispatch({
    type: PUT_STATICAL_ANNEX,
  });

  Axios.post(`technical_annex/add`, { ...newData })
    .then(({ data, status }) => {
      if (status !== 200) throw new Error();
      let newStatical = [...staticalAnnex, { ...data }].sort(
        (a, b) => a.page - b.page
      );
      dispatch({
        type: PUT_STATICAL_ANNEX_SUCCESS,
        payload: newStatical,
      });
    })
    .then(() => {
      dispatch({
        type: PUT_STATICAL_ANNEX_ERROR,
        payload: "",
      });
    });
};

export const updateStaticalAnnex = (newData) => (dispatch, getState) => {
  const { staticalAnnex = [] } = getState().axles;
  dispatch({
    type: UPDATE_STATICAL_ANNEX,
  });

  Axios.post(`technical_annex/update/${newData.id_technical_annex}`, {
    ...newData,
  })
    .then(({ status }) => {
      if (status !== 200) throw new Error();
      let nextS = staticalAnnex.map((item) => {
        if (item.id_technical_annex === newData.id_technical_annex) {
          return newData;
        }
        return item;
      });

      dispatch({
        type: UPDATE_STATICAL_ANNEX_SUCCESS,
        payload: nextS,
      });
    })
    .then(() => {
      dispatch({
        type: UPDATE_STATICAL_ANNEX_ERROR,
        payload: "",
      });
    });
};

export const deleteStaticalAnnex = (id_technical_annex) => (
  dispatch,
  getState
) => {
  const { staticalAnnex = [] } = getState().axles;
  dispatch({
    type: UPDATE_STATICAL_ANNEX,
  });

  Axios.get(`technical_annex/remove/${id_technical_annex}`)
    .then(({ status }) => {
      if (status !== 200) throw new Error();
      let nextS = staticalAnnex
        .filter((item) => item.id_technical_annex !== id_technical_annex)
        .sort((a, b) => a.page - b.page);

      dispatch({
        type: UPDATE_STATICAL_ANNEX_SUCCESS,
        payload: nextS,
      });
    })
    .then(() => {
      dispatch({
        type: UPDATE_STATICAL_ANNEX_ERROR,
        payload: "",
      });
    });
};

// PDF
export const getAllAxles = (force = false) => (dispatch, getState) => {
  const { axlesList = [] } = getState().axles;
  if (!force && axlesList.length > 0) return false;

  dispatch({
    type: GET_AXLES,
  });

  Axios.get(`axles/list`)
    .then(({ data, status }) => {
      if (status !== 200) throw new Error();
      dispatch({
        type: GET_AXLES_SUCCESS,
        payload: data.sort((a, b) => a.page - b.page),
      });
    })
    .catch(() => {
      dispatch({
        type: GET_AXLES_ERROR,
      });
    });
};

export const addAxles = (newData) => (dispatch, getState) => {
  const { axlesList = [] } = getState().axles;

  dispatch({
    type: PUT_AXLES,
  });

  Axios.post(`axles/add`, { ...newData })
    .then(({ data, status }) => {
      if (status !== 200) throw new Error();
      let newStatical = [...axlesList, { ...data }].sort(
        (a, b) => a.page - b.page
      );
      dispatch({
        type: PUT_AXLES_SUCCESS,
        payload: newStatical,
      });
    })
    .then(() => {
      dispatch({
        type: PUT_AXLES_ERROR,
        payload: "",
      });
    });
};

export const updateAxles = (newData) => (dispatch, getState) => {
  const { axlesList = [] } = getState().axles;
  dispatch({
    type: UPDATE_AXLES,
  });

  Axios.post(`axles/update`, {
    ...newData,
  })
    .then(({ status }) => {
      if (status !== 200) throw new Error();
      let nextS = axlesList.map((item) => {
        if (item.id_axles === newData.id_axles) {
          return newData;
        }
        return item;
      });

      dispatch({
        type: UPDATE_AXLES_SUCCESS,
        payload: nextS,
      });
    })
    .then(() => {
      dispatch({
        type: UPDATE_AXLES_ERROR,
        payload: "",
      });
    });
};

export const deleteAxles = (id_axles) => (dispatch, getState) => {
  const { axlesList = [] } = getState().axles;
  dispatch({
    type: UPDATE_AXLES,
  });

  Axios.get(`technical_annex/remove/${id_axles}`)
    .then(({ status }) => {
      if (status !== 200) throw new Error();
      let nextS = axlesList
        .filter((item) => item.id_axles !== id_axles)
        .sort((a, b) => a.page - b.page);

      dispatch({
        type: UPDATE_AXLES_SUCCESS,
        payload: nextS,
      });
    })
    .then(() => {
      dispatch({
        type: UPDATE_AXLES_ERROR,
        payload: "",
      });
    });
};
