import Axios from "axios";
import {
  GET_AUTH_STATUS,
  GET_AUTH_STATUS_ERROR,
  GET_AUTH_STATUS_SUCCESS,
  GET_USER_DATA,
  GET_USER_DATA_ERROR,
  GET_USER_DATA_SUCCESS,
  LOGGING,
  LOGGING_ERROR,
  LOGGING_SUCCESS,
  LOGOUT,
} from "../types/Auth";
import { deleteToken, readCookie, saveToken } from "../helpers";
import { apiUrl } from "../../constants/constants";

export const getAuthStatus = () => (dispatch, getState) => {
  // init app
  dispatch({
    type: GET_AUTH_STATUS,
  });

  var token = readCookie("token");
  var id_user = readCookie("id_user");

  if (!!token) {
    dispatch({
      type: GET_AUTH_STATUS_SUCCESS,
      payload: { token, id_user },
    });
    getUserData()(dispatch, getState);
  } else {
    dispatch({
      type: GET_AUTH_STATUS_ERROR,
      payload: "",
    });
  }
};

export const getUserData = () => (dispatch, getState) => {
  const { idUser, token } = getState().auth;
  dispatch({
    type: GET_USER_DATA,
  });
  Axios.get(`${apiUrl}users/info/${idUser}`, {
    headers: { Authorization: `Bearer ${token}` },
  })
    .then(({ status, data }) => {
      if (status !== 200) throw new Error(); // no admin
      if (data.role_id !== 1) throw new Error();
      dispatch({
        type: GET_USER_DATA_SUCCESS,
        payload: data,
      });
    })
    .catch(() => {
      dispatch({
        type: GET_USER_DATA_ERROR,
      });
      doLogout()(dispatch, getState);
    });
};

export const tryLogin = (email, password) => (dispatch, getState) => {
  dispatch({
    type: LOGGING,
  });

  Axios.post(`users/login`, {
    // Axios.post(`${apiUrl}users/login`, {
    email,
    password,
  })
    .then(async ({ data, status }) => {
      if (status !== 200) throw new Error();

      const isAdmin = await getUserPermission(data);

      if (!isAdmin) {
        return dispatch({
          type: LOGGING_ERROR,
          payload: "NO CUENTAS CON LOS PERMISOS NECESARIOS",
        });
      }
      saveToken(data);
      dispatch({
        type: LOGGING_SUCCESS,
        payload: data,
      });
      getUserData()(dispatch, getState);
    })
    .catch(() => {
      dispatch({
        type: LOGGING_ERROR,
        payload: "Ocurrió un error, intenta más tarde.",
      });
    });
};

export const loginFb = (fb_access_token) => (dispatch, getState) => {
  dispatch({
    type: LOGGING,
  });

  Axios.post(`users/login/facebook/mobile`, {
    fb_access_token,
  })
    .then(async ({ data, status }) => {
      if (status !== 200) throw new Error();

      const isAdmin = await getUserPermission(data);

      if (!isAdmin) {
        return dispatch({
          type: LOGGING_ERROR,
          payload: "NO CUENTAS CON LOS PERMISOS NECESARIOS",
        });
      }
      saveToken(data);
      dispatch({
        type: LOGGING_SUCCESS,
        payload: data,
      });
      getUserData()(dispatch, getState);
    })
    .catch(() => {
      dispatch({
        type: LOGGING_ERROR,
        payload: "Ocurrió un error, intenta más tarde.",
      });
    });
};

export const loginGoogle = (tokenId) => (dispatch, getState) => {
  dispatch({
    type: LOGGING,
  });

  Axios.post(`users/login_google`, {
    token: tokenId,
  })
    .then(async ({ status, data }) => {
      if (status !== 200) throw new Error();

      const isAdmin = await getUserPermission(data);

      if (!isAdmin) {
        return dispatch({
          type: LOGGING_ERROR,
          payload: "NO CUENTAS CON LOS PERMISOS NECESARIOS",
        });
      }
      saveToken(data);
      dispatch({
        type: LOGGING_SUCCESS,
        payload: data,
      });
      getUserData()(dispatch, getState);
    })
    .catch(() => {
      dispatch({
        type: LOGGING_ERROR,
        payload: "Ocurrió un error, intenta más tarde.",
      });
    });
};

export const loginApple = (tokenId) => (dispatch, getState) => {
  dispatch({
    type: LOGGING,
  });

  Axios.post(`users/login_apple`, {
    token: tokenId,
  })
    .then(async ({ status, data }) => {
      if (status !== 200) throw new Error();

      const isAdmin = await getUserPermission(data);

      if (!isAdmin) {
        return dispatch({
          type: LOGGING_ERROR,
          payload: "NO CUENTAS CON LOS PERMISOS NECESARIOS",
        });
      }
      saveToken(data);
      dispatch({
        type: LOGGING_SUCCESS,
        payload: data,
      });
      getUserData()(dispatch, getState);
    })
    .catch(() => {
      dispatch({
        type: LOGGING_ERROR,
        payload: "Ocurrió un error, intenta más tarde.",
      });
    });
};

export const doLogout = () => (dispatch) => {
  dispatch({ type: LOGOUT });
  deleteToken();
};

const getUserPermission = async (dat) => {
  const { token, id_user } = dat;
  try {
    const { status, data } = await Axios.get(`${apiUrl}users/info/${id_user}`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    if (status !== 200) return false;
    return data.role_id === 1;
  } catch (e) {
    return false;
  }
};
