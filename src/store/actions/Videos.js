import Axios from "axios";
import {
  GET_STREAMS,
  GET_STREAMS_ERROR,
  GET_STREAMS_SUCCESS,
  GET_VIDEOS,
  GET_VIDEOS_ERROR,
  GET_VIDEOS_SUCCESS,
  PUT_STREAM,
  PUT_STREAM_ERROR,
  PUT_STREAM_SUCCESS,
  PUT_VIDEO,
  PUT_VIDEO_ERROR,
  PUT_VIDEO_SUCCESS,
  UPDATE_MEDIA,
  UPDATE_MEDIA_RESPONSE,
  UPDATE_STREAM,
  UPDATE_STREAM_SUCCESS,
} from "../types/Video";
import { apiUrl } from "../../constants/constants";
import moment from "moment";

export const getAllVideos = (force = false) => (dispatch, getState) => {
  const { videosList = [] } = getState().videos;

  if (!force && videosList.length > 0) return false;

  dispatch({
    type: GET_VIDEOS,
  });

  Axios.get(`${apiUrl}videos/list`)
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_VIDEOS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_VIDEOS_ERROR,
        payload: "",
      });
    });
};

export const deleteVideo = (id_video) => (dispatch, getState) => {
  const { videosList = [] } = getState().videos;
  const newVideos = videosList.filter((item) => item.id_video !== id_video);

  Axios.get(`${apiUrl}videos/remove/${id_video}`).then(({ status }) => {
    if (status === 200) {
      dispatch({
        type: GET_VIDEOS_SUCCESS,
        payload: newVideos,
      });
    }
  });
};

export const AddVideo = (info, callBack) => (dispatch, getState) => {
  const { title, url, image } = info;
  const { videosList = [] } = getState().videos;
  dispatch({
    type: PUT_VIDEO,
  });

  let toSendData = new FormData();

  toSendData.append("title", title);
  toSendData.append("url", url);
  toSendData.append("image", image);

  Axios.post(`${apiUrl}videos/add`, toSendData)
    .then(({ status, data }) => {
      if (status === 200) {
        const newVideos = [...videosList, { ...data }];
        dispatch({
          type: PUT_VIDEO_SUCCESS,
          payload: newVideos,
        });
        callBack(data.id_video);
      }
    })
    .catch(() => {
      dispatch({
        type: PUT_VIDEO_ERROR,
      });
    });
};

export const updateVideo = (info, callBack) => (dispatch, getState) => {
  const { title, url, image, id_video } = info;

  dispatch({
    type: UPDATE_MEDIA,
  });

  setTimeout(() => {
    let toSendData = new FormData();
    toSendData.append("title", title);
    toSendData.append("url", url);
    if (image) {
      toSendData.append("image", image);
    }

    Axios.post(`videos/update/${id_video}`, toSendData)
      .then(() => {
        getAllVideos(true)(dispatch, getState);
        dispatch({
          type: UPDATE_MEDIA_RESPONSE,
        });
        callBack();
      })
      .catch(() => {
        dispatch({
          type: UPDATE_MEDIA_RESPONSE,
        });
        callBack();
      });
  }, 2000);
};

export const getAllStreams = (force = false) => (dispatch, getState) => {
  const { streamsList = [] } = getState().videos;

  if (!force && streamsList.length > 0) return false;

  dispatch({
    type: GET_STREAMS,
  });

  Axios.get(`${apiUrl}live_cast/list`)
    .then(({ data, status }) => {
      if (status !== 200) throw new Error();
      return dispatch({
        type: GET_STREAMS_SUCCESS,
        payload: data,
      });
    })
    .catch(() => {
      dispatch({
        type: GET_STREAMS_ERROR,
        payload: "",
      });
    });
};

export const addLiveCast = (dat, callBack = () => {}) => (
  dispatch,
  getState
) => {
  const { streamsList = [] } = getState().videos;

  dispatch({
    type: PUT_STREAM,
  });

  let date = moment(dat.date);

  let body = new FormData();
  body.append("url", dat.url);
  body.append("title", dat.title);
  body.append("date", date.format("YYYY-MM-DD"));
  body.append("active", 1);
  body.append("image", dat.thumb);

  Axios.post(`live_cast/add`, body)
    .then(({ status, data }) => {
      if (status !== 200) throw new Error();

      let newLiveCast = [...streamsList, { ...data }];
      dispatch({
        type: PUT_STREAM_SUCCESS,
        payload: newLiveCast,
      });

      callBack(data.id_live_cast);
    })
    .catch(() => {
      dispatch({
        type: PUT_STREAM_ERROR,
        payload: "",
      });
    });
};

export const deleteStream = (id_live_cast) => (dispatch, getState) => {
  dispatch({
    type: UPDATE_STREAM,
  });

  const { streamsList = [] } = getState().videos;

  Axios.get(`live_cast/remove/${id_live_cast}`)
    .then(({ status }) => {
      if (status !== 200) throw new Error();
      const filteredStreams = streamsList.filter(
        (item) => item.id_live_cast !== id_live_cast
      );

      dispatch({
        type: UPDATE_STREAM_SUCCESS,
        payload: filteredStreams,
      });
    })
    .catch(() => {
      dispatch({
        type: UPDATE_STREAM_SUCCESS,
        payload: "",
      });
    });
};

export const updateStream = (dat, callBack = () => {}) => (
  dispatch,
  getState
) => {
  const { streamsList = [] } = getState().videos;

  dispatch({
    type: PUT_STREAM,
  });

  let date = moment(dat.date);

  let body = new FormData();
  body.append("url", dat.url);
  body.append("title", dat.title);
  body.append("date", date.format("YYYY-MM-DD"));
  body.append("active", 1);
  if (dat.thumb) {
    body.append("thum", dat.thumb);
  }

  Axios.post(`live_cast/update/${dat.id_live_cast}`, body)
    .then(({ status, data }) => {
      if (status !== 200) throw new Error();

      let newLiveCast = streamsList.map((item) => {
        if (item.id_live_cast === dat.id_live_cast) {
          return dat;
        }
        return item;
      });
      dispatch({
        type: PUT_STREAM_SUCCESS,
        payload: newLiveCast,
      });

      callBack(data.id_live_cast);
    })
    .catch(() => {
      dispatch({
        type: PUT_STREAM_ERROR,
        payload: "",
      });
    });
};
