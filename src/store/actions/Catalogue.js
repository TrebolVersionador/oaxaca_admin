import Axios from "axios";
import {
  GET_AREAS,
  GET_AREAS_ERROR,
  GET_AREAS_SUCCESS,
  GET_LOCATIONS,
  GET_LOCATIONS_SUCCESS,
  GET_LOCATIONS_ERROR,
  GET_MUNICIPIOS,
  GET_MUNICIPIOS_SUCCESS,
  GET_MUNICIPIOS_ERROR,
  GET_TITLES,
  GET_TITLES_SUCCESS,
  GET_TITLES_ERROR,
  GET_REGIONS,
  GET_REGIONS_SUCCESS,
  GET_REGIONS_ERROR,
} from "../types/Catalogues";

export const getAreas = (force = false) => (dispatch, getState) => {
  const { areas = [] } = getState().catalogue;
  if (!force && areas.length > 0) return false;

  dispatch({
    type: GET_AREAS,
  });

  Axios.get("catalogs/officer_areas")
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_AREAS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_AREAS_ERROR,
        payload: "",
      });
    });
};

export const getLocations = (force = false) => (dispatch, getState) => {
  const { locations = [] } = getState().catalogue;
  if (!force && locations.length > 0) return false;

  dispatch({
    type: GET_LOCATIONS,
  });

  Axios.get("catalogs/locations")
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_LOCATIONS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_LOCATIONS_ERROR,
        payload: "",
      });
    });
};

export const getMunicipios = (force = false) => (dispatch, getState) => {
  const { municipios = [] } = getState().catalogue;
  if (!force && municipios.length > 0) return false;

  dispatch({
    type: GET_MUNICIPIOS,
  });

  Axios.get("catalogs/municipios")
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_MUNICIPIOS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_MUNICIPIOS_ERROR,
        payload: "",
      });
    });
};

export const getTitles = (force = false) => (dispatch, getState) => {
  const { titles = [] } = getState().catalogue;
  if (!force && titles.length > 0) return false;

  dispatch({
    type: GET_TITLES,
  });

  Axios.get("catalogs/officers_titles")
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_TITLES_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_TITLES_ERROR,
        payload: "",
      });
    });
};

export const getRegions = (force = true) => (dispatch, getState) => {
  const { titles = [] } = getState().catalogue;
  if (!force && titles.length > 0) return false;

  dispatch({
    type: GET_REGIONS,
  });

  Axios.get("catalogs/regions")
    .then(({ data, status }) => {
      if (status === 200) {
        return dispatch({
          type: GET_REGIONS_SUCCESS,
          payload: data,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: GET_REGIONS_ERROR,
        payload: "",
      });
    });
};
