import Axios from "axios";
import {
  GET_ALL_USERS,
  GET_ALL_USERS_ERROR,
  GET_ALL_USERS_SUCCESS,
  UPDATE_USER,
  UPDATE_USER_ERROR,
  UPDATE_USER_SUCCESS,
} from "../types/Users";

export const getUsers = (force = false) => (dispatch, getState) => {
  const { users = [] } = getState().users;
  if (!force && users.length > 0) return false;

  dispatch({
    type: GET_ALL_USERS,
  });

  Axios.get("users/list_users")
    .then(({ status, data }) => {
      if (status !== 200) throw new Error();
      dispatch({
        type: GET_ALL_USERS_SUCCESS,
        payload: data,
      });
    })
    .catch(() => {
      dispatch({
        type: GET_ALL_USERS_ERROR,
      });
    });
};

export const updateUser = (newUser, callback = () => {}) => (
  dispatch,
  getState
) => {
  const { users = [] } = getState().users;

  dispatch({
    type: UPDATE_USER,
  });

  Axios.post(`users/update/${newUser.id_user}`, { ...newUser })
    .then(({ status }) => {
      if (status !== 200) throw new Error();

      const nextUsers = users.map((item) => {
        if (item.id_user === newUser.id_user) {
          return { ...newUser };
        }
        return item;
      });
      dispatch({
        type: UPDATE_USER_SUCCESS,
        payload: nextUsers,
      });

      callback();
    })
    .catch(() => {
      dispatch({
        type: UPDATE_USER_ERROR,
        payload: "",
      });
    });
};
