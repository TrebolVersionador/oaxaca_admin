import Axios from "axios";
import {
  GET_OFFICERS,
  GET_OFFICERS_ERROR,
  GET_OFFICERS_SUCCESS,
  PUT_OFFICER,
  PUT_OFFICER_ERROR,
  PUT_OFFICER_SUCCESS,
} from "../types/Directory";
import { apiUrl } from "../../constants/constants";

export const getAllDirectory = (force = false) => (dispatch, getState) => {
  const { officers } = getState().directory;
  if (!force && officers.length > 0) return false;
  dispatch({
    type: GET_OFFICERS,
  });

  Axios.get(`${apiUrl}officers/list`)
    .then(({ data, status }) => {
      if (status === 200) {
        let directory = [];
        Object.keys(data).map((x) => {
          return (directory = [...directory, ...data[x]]);
        });
        dispatch({
          type: GET_OFFICERS_SUCCESS,
          payload: directory,
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_OFFICERS_ERROR,
        payload: "err",
      });
    });
};

export const addOfficer = (data, callBack) => (dispatch, getState) => {
  Axios.post(`officers/add`, { ...data }).then(({ status, data }) => {
    if (status === 200) {
      getAllDirectory(true)(dispatch, getState);
      callBack(data.id_officer);
    }
  });
};

export const editOfficer = (data, callBack) => (dispatch, getState) => {
  Axios.post(`officers/update`, { ...data }).then(({ status, data }) => {
    if (status === 200) {
      getAllDirectory(true)(dispatch, getState);
      callBack(data.id_officer);
    }
  });
};

export const toggleDeleteOfficer = (id_officer, newVal) => (
  dispatch,
  getState
) => {
  const { officers = [] } = getState().directory;

  dispatch({
    type: PUT_OFFICER,
  });
  Axios.post(`officers/disable`, { id_officer })
    .then(({ status }) => {
      if (status === 200) {
        let nextOfficers = officers.map((item) => {
          if (item.id_officer === id_officer) {
            return { ...item, active: newVal };
          }
          return item;
        });

        dispatch({
          type: PUT_OFFICER_SUCCESS,
          payload: nextOfficers,
        });
      }
      throw new Error();
    })
    .catch(() => {
      dispatch({
        type: PUT_OFFICER_ERROR,
        payload: "",
      });
    });
};
