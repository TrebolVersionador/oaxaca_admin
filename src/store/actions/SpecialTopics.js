import Axios from "axios";
import {
  GET_TOPICS,
  GET_TOPICS_ERROR,
  GET_TOPICS_SUCCESS,
  PUT_TOPICS,
  PUT_TOPICS_SUCCESS,
} from "../types/SpecialTopics";

export const getAllTopics = (force = false) => (dispatch, getState) => {
  const { topics = [] } = getState().topics;
  if (!force && topics.length > 0) return false;
  dispatch({
    type: GET_TOPICS,
  });
  Axios.get("special_topics/list")
    .then(({ status, data }) => {
      if (status !== 200) throw new Error();

      dispatch({
        type: GET_TOPICS_SUCCESS,
        payload: data,
      });
    })
    .catch(() => {
      dispatch({
        type: GET_TOPICS_ERROR,
      });
    });
};

export const disableSpecialTopic = (id_special_topics, newVal) => (
  dispatch,
  getState
) => {
  const { topics } = getState().topics;

  Axios.post(`special_topics/disable`, { id_special_topics }).then(
    ({ status }) => {
      if (status === 200) {
        let newTopics = topics.map((item) => {
          if (item.id_special_topics === id_special_topics) {
            return { ...item, active: newVal };
          }
          return item;
        });
        dispatch({
          type: PUT_TOPICS_SUCCESS,
          payload: newTopics,
        });
      }
    }
  );
};

export const updateSpecialTopic = (special_topics, callback = () => {}) => (
  dispatch,
  getState
) => {
  dispatch({
    type: PUT_TOPICS,
  });
  const { topics } = getState().topics;
  Axios.post(`special_topics/update`, { ...special_topics }).then(
    ({ status }) => {
      if (status === 200) {
        let newTopics = topics.map((item) => {
          if (item.id_special_topics === special_topics.id_special_topics) {
            return { ...special_topics };
          }
          return item;
        });
        dispatch({
          type: PUT_TOPICS_SUCCESS,
          payload: newTopics,
        });
        callback();
      }
    }
  );
};

export const deleteTopicImage = async (item) => {
  try {
    const { status, data } = await Axios.post(`special_topics/delete_image`, {
      id_image: item.id_image,
    });
    console.log(data);
    return status === 200;
  } catch (e) {
    return false;
  }
};

export const addSpecialTopic = (nextData, images, callback) => (
  dispatch,
  getState
) => {
  dispatch({
    type: GET_TOPICS,
  });
  Axios.post("special_topics/add", { ...nextData })
    .then(({ data, status }) => {
      if (status === 200) {
        addTopicImage(images, data.id_special_topics, () => {
          getAllTopics(true)(dispatch, getState);
          callback(data.id_special_topics);
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_TOPICS_ERROR,
      });
    });
};

export const addTopicImage = (images, id_special_topics, callBack) => {
  let services = [];

  Object.keys(images).map((x) => {
    let formData = new FormData();
    formData.append("id_special_topics", id_special_topics);
    formData.append("url", images[x]);
    services.push(Axios.post(`special_topics/add_image`, formData));
    callBack();
    return "";
  });

  Axios.all(services);
};
