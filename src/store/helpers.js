export function saveToken(data) {
  document.cookie = `token=${encodeURIComponent(data.token)}`;
  document.cookie = `id_user=${encodeURIComponent(data.id_user)}`; // document.cookie = `id_user=${encodeURIComponent(data.id_user)}`;
}

export function readCookie(name) {
  return document.cookie.replace(
    new RegExp(
      "(?:(?:^|.*;)\\s*" +
        name.replace(/[-.+*]/g, "\\$&") +
        "\\s*\\=\\s*([^;]*).*$)|^.*$"
    ),
    "$1"
  );
}

export function deleteToken() {
  document.cookie = `token=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
  document.cookie = `id_user=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
}
