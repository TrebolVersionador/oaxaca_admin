export const GET_NEWS = "GET_NEWS";
export const GET_NEWS_SUCCESS = "GET_NEWS_SUCCESS";
export const GET_NEWS_ERROR = "GET_NEWS_ERROR";

export const PUT_NEW = "PUT_NEW";
export const PUT_NEW_SUCCESS = "PUT_NEW_SUCCESS";
export const PUT_NEW_ERROR = "PUT_NEW_ERROR";

export const UPDATE_NEW = "UPDATE_NEW";
export const UPDATE_NEW_SUCCESS = "UPDATE_NEW_SUCCESS";
export const UPDATE_NEW_ERROR = "UPDATE_NEW_ERROR";

export const GET_INFO = "GET_INFO";
export const GET_INFO_SUCCESS = "GET_INFO_SUCCESS";
export const GET_INFO_ERROR = "GET_INFO_ERROR";
