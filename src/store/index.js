import { Provider } from "react-redux";
import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducers/index";

const allEnhancers = compose(
  applyMiddleware(thunk)
  // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

function configureStore(preloadedState = {}) {
  const store = createStore(
    reducer, // root reducer with router state
    preloadedState,
    allEnhancers
  );
  return store;
}
const store = configureStore();
// store;
export default function ReduxWrapper({ children }) {
  return <Provider store={store}>{children}</Provider>;
}
